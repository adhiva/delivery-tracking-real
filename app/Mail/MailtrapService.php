<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailtrapService extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $data = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->custom_config_mail = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->custom_config_mail['mail_from'], $this->custom_config_mail['name_from'])
        ->subject($this->custom_config_mail['mail_subject'])
        ->markdown('mail.service_mail')
        ->with([
            'name' => $this->custom_config_mail['name_from'],
            'link' => 'https://mailtrap.io/inboxes',
            'file_name' => $this->custom_config_mail['file_name'],
            'sttb_number' => $this->custom_config_mail['sttb_number']
        ]);
        // return $this->view('view.name');
    }
}
