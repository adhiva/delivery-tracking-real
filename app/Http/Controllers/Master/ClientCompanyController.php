<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ClientCompany;
use App\Models\DeliveryTransaction;
use Datatables;

class ClientCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.client_company_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        $client_company = ClientCompany::create($data);
        if (!$client_company) {
            $message = [
                'message' => 'Failed save client company !'
            ];
            return response()->json($message, 422);
        }
        $message = [
            'message' => 'Success save client company'
        ];
        return response()->json($message, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ClientCompany::find($id);
        $message = [
            'message' => 'Success',
            'data' => $data
        ];
        return response()->json($message, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client_company = ClientCompany::find($id);
        $client_company->name = $request->input('name');
        $client_company->address = $request->input('address');
        $client_company->contact = $request->input('contact');
        $client_company->save();

        $message = [
            'message' => 'Success update client company!'
        ];
        return response()->json($message, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client_company = ClientCompany::findOrFail($id); 
        $client_company->delete();
        $message = [
            'message' => 'Success remove client company!'
        ];
        return response()->json($message, 200);
    }

    /** 
     * @Author: Adhitya Giva Muhammad
     * @Date: 2019-10-15 15:53:09 
     * @Desc:  Return datatable
     */    
    public function anyData()
    {
        $data = ClientCompany::all();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = 
                    '<a href="javascript:void(0)" onclick="updateClientCompany(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Edit Client Company" class="edit btn waves-effect waves-light btn-info">
                        <i class="fa fas fa-pencil-alt"></i>
                    </a> ';
                    $btn .= 
                    '<a href="javascript:void(0)" onclick="removeClientCompany(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Delete Client Company" class="delete btn waves-effect waves-light btn-danger">
                        <i class="fa fas fa-trash-alt"></i>
                    </a> ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    /** 
     * @Author: Adhitya Giva Muhammad 
     * @Date: 2019-11-13 20:54:17 
     * @Desc: For select2 only in delivery order
     */    
    public function loadData(Request $request)
    {
        if ($request->has('q')) {
            $search = $request->query('q');
            $data = ClientCompany::select("id","name")
                ->where('type','LIKE',"%$search%")
                ->get()
            ;
            return response()->json($data);
        }
           
        $data = ClientCompany::select("id","name")
            ->get()
        ;
        return response()->json($data);
    }
}
