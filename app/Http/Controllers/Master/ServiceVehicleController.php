<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Courier;
use Illuminate\Http\Request;

use App\Models\ServiceVehicle;
use Illuminate\Support\Facades\DB;

use Datatables;
use Illuminate\Support\Facades\Auth;

class ServiceVehicleController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('master.service_vehicle_list');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$user_id = Auth::user()->id;
		$getCourierId = Courier::where('user_id', $user_id)->pluck('id')->first();
		$data = $request->input();
		$data['courier_id'] = $getCourierId;
		$service_vehicle = ServiceVehicle::create($data);
		if (!$service_vehicle) {
			$message = [
				'message' => 'Failed save service vehicle!'
			];
			return response()->json($message, 422);
		}
		$message = [
			'message' => 'Success save service vehicle!'
		];
		return response()->json($message, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$data = DB::table('view_service_vehicle')->where('id', $id)->first();
		$message = [
			'message' => 'Success',
			'data' => $data
		];
		return response()->json($message, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$service_vehicle = ServiceVehicle::find($id);
		$service_vehicle->complaint = $request->input('complaint');
		if ($request->has('service_date')) {
			$service_vehicle->service_date = $request->input('service_date');
		}
		$service_vehicle->save();

		$message = [
			'message' => 'Success update service vehicle!'
		];
		return response()->json($message, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$service_vehicle = ServiceVehicle::findOrFail($id); 
		$service_vehicle->delete();
		$message = [
			'message' => 'Success remove service vehicle!'
		];
		return response()->json($message, 200);
	}

	/** 
	 * @Author: Adhitya Giva Muhammad 
	 * @Date: 2019-11-13 21:08:53 
	 * @Desc: Update status service vehicle
	 */	
	public function updateStatusService(Request $request, $id)
	{
		$status = $request->input('status');
		$service_vehicle = ServiceVehicle::find($id);

		switch ($status) {
			case "0":
				$message_desc = "Service vehicle approved!";
				$service_vehicle->service_date = $request->input('service_date');
				$service_vehicle->status = 1;
				break;
			case "1":
				$message_desc = "Finish service vehicle!";
				$service_vehicle->finish_service_date = \date("Y-m-d H:i:s", \time());
				$service_vehicle->status = 2;
				break;
			default:
				$message_desc = "Failed get status";
				$service_vehicle->service_date = null;
				$service_vehicle->finish_service_date = null;
				$service_vehicle->status = 0;
				break;
		}
		
		$service_vehicle->save();

		$message = [
			'message' => $message_desc
		];
		return response()->json($message, 200);
	}

	/** 
	 * @Author: Adhitya Giva Muhammad
	 * @Date: 2019-10-15 15:53:09 
	 * @Desc:  Return datatable
	 */    
	public function anyData()
	{
		// $data = ServiceVehicle::all();
		$data = DB::table('view_service_vehicle')->get();
		
		// Get data courier
		$user = Auth::user();
		$userId = $user->id;
		$getCourierId = Courier::where("user_id", "=", $userId)->pluck("id")->first();
		if ($user->getRoleNames()[0] != "admin") {
			$data = DB::table('view_service_vehicle')->where("courier_id", "=", $getCourierId)->get();
		}
		return Datatables::of($data)
				->addIndexColumn()
				->editColumn('service_date', function($data) {
					return ($data->service_date == null || $data->service_date == "") ? "Not Set" : $data->service_date;
				})
				->editColumn('finish_service_date', function($data) {
					return ($data->finish_service_date == null || $data->finish_service_date == "") ? "Not Set" : $data->service_date;
				})
				->addColumn('action', function($row){
					$user = Auth::user();

					$btn = "";
					if ($row->status != 2) {
						$btn .= 
						'<a href="javascript:void(0)" onclick="updateServiceVehicle(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Edit Service Vehicle" class="edit btn waves-effect waves-light btn-info">
							<i class="fa fas fa-pencil-alt"></i>
						</a> ';
					}
					
					
					if ($user->hasRole('admin') && $row->status == 0) {
						$btn .= 
						'<a href="javascript:void(0)" onclick="approveServiceVehicle(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Approve Service Vehicle" class="delete btn waves-effect waves-light btn-success">
							<i class="fa fas fa-clipboard-check"></i>
						</a> ';
					}

					if ($user->hasRole('admin') && $row->status == 1) {
						$btn .= 
						'<a href="javascript:void(0)" onclick="updateStatusToFinishService(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Finish Service" class="delete btn waves-effect waves-light btn-primary">
							<i class="fa fas fa-check"></i>
						</a> ';
					}

					$btn .= 
					'<a href="javascript:void(0)" onclick="removeServiceVehicle(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Delete Service Vehicle" class="delete btn waves-effect waves-light btn-danger">
						<i class="fa fas fa-trash-alt"></i>
					</a> ';
					return $btn;
				})
				->rawColumns(['action'])
				->make(true);
	}
}
