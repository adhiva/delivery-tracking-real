<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Items;
use Datatables;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.item_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->except('price_format');
        $item = Items::create($data);
        if (!$item) {
            $message = [
                'message' => 'Failed save item !'
            ];
            return response()->json($message, 422);
        }
        $message = [
            'message' => 'Success save item'
        ];
        return response()->json($message, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Items::find($id);
        $message = [
            'message' => 'Success',
            'data' => $data
        ];
        return response()->json($message, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Items::find($id);
        $item->name = $request->input('name');
        $item->price = $request->input('price');
        $item->quantity = $request->input('quantity');
        $item->save();

        $message = [
            'message' => 'Success update item!'
        ];
        return response()->json($message, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Items::findOrFail($id); 
        $item->delete();
        $message = [
            'message' => 'Success remove item!'
        ];
        return response()->json($message, 200);
    }

    /** 
     * @Author: Adhitya Giva Muhammad
     * @Date: 2019-10-15 15:53:09 
     * @Desc:  Return datatable
     */    
    public function anyData()
    {
        $data = Items::all();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = 
                    '<a href="javascript:void(0)" onclick="updateItem(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Edit User" class="edit btn waves-effect waves-light btn-info">
                        <i class="fa fas fa-pencil-alt"></i>
                    </a> ';
                    $btn .= 
                    '<a href="javascript:void(0)" onclick="removeItem(\''.$row->id.'\');" data-toggle="tooltip" data-placement="top" title="Delete User" class="delete btn waves-effect waves-light btn-danger">
                        <i class="fa fas fa-trash-alt"></i>
                    </a> ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    /** 
     * @Author: Adhitya Giva Muhammad 
     * @Date: 2019-11-13 20:54:17 
     * @Desc: For select2 only in delivery order
     */    
    public function loadData(Request $request)
    {
        # Get data vehicle id in service table
        # filtering it
        if ($request->has('q')) {
            $search = $request->query('q');
            $data = Items::where('type','LIKE',"%$search%")
                ->where('quantity', '!=', 0)
                ->get()
            ;
            return response()->json($data);
        }
        
        $data = Items::where('quantity', '!=', 0)
            ->get()
        ;
        return response()->json($data);
    }
}
