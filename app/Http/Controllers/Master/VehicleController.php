<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Vehicle;
use App\Models\ServiceVehicle;
use App\Models\DeliveryTransaction;
use Datatables;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.vehicle_list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Vehicle::find($id);
        $message = [
            'message' => 'Success',
            'data' => $data
        ];
        return response()->json($message, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        $Vehicle = Vehicle::create($data);
        if (!$Vehicle) {
            $message = [
                'message' => 'Failed save Vehicle !'
            ];
            return response()->json($message, 422);
        }
        $message = [
            'message' => 'Success save Vehicle'
        ];
        return response()->json($message, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Vehicle = Vehicle::find($id);
        $Vehicle->number_plate = $request->input('number_plate');
        $Vehicle->merk = $request->input('merk');
        $Vehicle->type = $request->input('type');
        $Vehicle->capacity = $request->input('capacity');
        $Vehicle->pcs = $request->input('pcs');
        $Vehicle->service_schedule_monthly = $request->input('service_schedule_monthly');
        $Vehicle->save();

        $message = [
            'message' => 'Success update Vehicle !'
        ];
        return response()->json($message, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Vehicle = Vehicle::findOrFail($id);
        $Vehicle->delete();
        $message = [
            'message' => 'Success remove vehicle !'
        ];
        return response()->json($message, 200);
    }

    /**
     * @Author: Adhitya Giva Muhammad
     * @Date: 2019-10-15 15:53:09
     * @Desc:  Return Vehicle data to datatable
     */
    public function anyData()
    {
        $data = Vehicle::all();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn =
                    '<a href="javascript:void(0)" onclick="updateVehicle(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Update Vehicle" class="edit btn waves-effect waves-light btn-info">
                        <i class="fa fas fa-pencil-alt"></i>
                    </a> ';
                    $btn .=
                    '<a href="javascript:void(0)" onclick="removeVehicle(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Delete Vehicle" class="delete btn waves-effect waves-light btn-danger">
                        <i class="fa fas fa-trash-alt"></i>
                    </a> ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    /**
     * @Author: Adhitya Giva Muhammad
     * @Date: 2019-11-13 20:54:17
     * @Desc: For select2 only in service vehicle and delivery order
     */
    public function loadData(Request $request)
    {
        # Get data vehicle id in service table
        # filtering it
        $data_vehicle_id = ServiceVehicle::select("vehicle_id")
            ->where('status', '!=' , 2)
            ->get()
            ->toArray();

        // when get data from do
        if ($request->has('is_do')) {
            $data_vehicle_id = DeliveryTransaction::select("vehicle_id")
                ->where('status', '!=' , 1)
                ->get()
                ->toArray();
        }

        if ($request->has('q')) {
            $search = $request->query('q');
            $data = Vehicle::select("id","type", "capacity")
                ->where('type','LIKE',"%$search%")
                ->whereNotIn('id', $data_vehicle_id)
                ->get()
            ;
            return response()->json($data);
        }

        $data = Vehicle::select("id","type", "capacity")
            ->whereNotIn('id', $data_vehicle_id)
            ->get()
        ;
        return response()->json($data);
    }

}
