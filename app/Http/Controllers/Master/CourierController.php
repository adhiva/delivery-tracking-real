<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Courier;
use App\Models\DeliveryTransaction;
use Datatables;

class CourierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.courier_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        $courier = Courier::create($data);
        if (!$courier) {
            $message = [
                'message' => 'Failed save courier !'
            ];
            return response()->json($message, 422);
        }
        $message = [
            'message' => 'Success save courier'
        ];
        return response()->json($message, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Courier::find($id);
        $message = [
            'message' => 'Success',
            'data' => $data
        ];
        return response()->json($message, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $courier = Courier::find($id);
        $courier->name = $request->input('name');
        $courier->number_sim = $request->input('number_sim');
        $courier->number_identity = $request->input('number_identity');
        $courier->gender = $request->input('gender');
        $courier->date_of_birth = $request->input('date_of_birth');
        $courier->address = $request->input('address');
        $courier->save();

        $message = [
            'message' => 'Success update courier!'
        ];
        return response()->json($message, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $courier = Courier::findOrFail($id); 
        $courier->delete();
        $message = [
            'message' => 'Success remove courier!'
        ];
        return response()->json($message, 200);
    }

    /** 
     * @Author: Adhitya Giva Muhammad
     * @Date: 2019-10-15 15:53:09 
     * @Desc:  Return datatable
     */    
    public function anyData()
    {
        $data = Courier::all();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = 
                    '<a href="javascript:void(0)" onclick="updateCourier(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Edit Courier" class="edit btn waves-effect waves-light btn-info">
                        <i class="fa fas fa-pencil-alt"></i>
                    </a> ';
                    $btn .= 
                    '<a href="javascript:void(0)" onclick="removeCourier(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Delete Courier" class="delete btn waves-effect waves-light btn-danger">
                        <i class="fa fas fa-trash-alt"></i>
                    </a> ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    /** 
     * @Author: Adhitya Giva Muhammad 
     * @Date: 2019-11-13 20:54:17 
     * @Desc: For select2 only in delivery order
     */    
    public function loadData(Request $request)
    {
        # Get data vehicle id in service table
        # filtering it
        $courierInTransaction = DeliveryTransaction::select("courier_id")
            ->where('status', '!=' , 1)
            ->get()
            ->toArray();

        if ($request->has('q')) {
            $search = $request->query('q');
            $data = Courier::select("id","name")
                ->where('name','LIKE',"%$search%")
                ->whereNotIn('id', $courierInTransaction)
                ->get()
            ;
            return response()->json($data);
        }
        
        $data = Courier::select("id","name")
            ->whereNotIn('id', $courierInTransaction)
            ->get()
        ;
        return response()->json($data);
    }

    public function loadDataCourierAsUser(Request $request)
    {
        if ($request->has('q')) {
            $search = $request->query('q');
            $data = Courier::select("id","name")
                ->where('name','LIKE',"%$search%")
                ->whereNull('user_id')
                ->get()
            ;
            return response()->json($data);
        }
        
        $data = Courier::select("id","name")
            ->whereNull('user_id')
            ->get()
        ;
        return response()->json($data);
    }

    public function addUserToCourier(Request $request)
    {
        $courier_id = $request->input('courier_id');
        $courier = Courier::find($courier_id);
        $courier->user_id = $request->input('user_id');
        $courier->save();

        $message = [
            'message' => 'Success assign user to courier!'
        ];
        return response()->json($message, 200);
    }
}
