<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

use App\Models\DeliveryTransaction;
use App\Models\DeliveryTransactionDetail;
use App\Models\Courier;
use Illuminate\Support\Facades\DB;
use App\Traits\AutoNumberTransaction;

use Datatables;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;
use App\Jobs\SendEmail;
use PDF;

class DeliveryOrderController extends Controller
{
	use AutoNumberTransaction;
    /**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('transaction.delivery_order');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		DB::beginTransaction();
		try {
			$deliveryTransactionData = $request->except('item_id','harga','quantity');
			$deliveryTransactionData['sttb_number'] = $this->getTransactionNumber('STTB');
			$deliveryTransactionData['do_number'] = $this->getTransactionNumber('DO');

			$newDeliveryTransaction = DeliveryTransaction::create($deliveryTransactionData);
			$items = $request->input('item_id');
			$quantity = $request->input('quantity');

			foreach ($items as $key => $item) {
				$createDeliveryTransactionDetail = DeliveryTransactionDetail::create(
					[
						'delivery_transaction_id' => $newDeliveryTransaction->id,
						'item_id' => $item,
						'quantity' => $quantity[$key],
					]
				);
			}

			if ($newDeliveryTransaction && $createDeliveryTransactionDetail) {
				$message = [
					'message' => 'Success save delivery transaction!'
				];
				DB::commit();
				return response()->json($message, 200);
			}

		} catch (\Exception $e) {
			DB::rollBack();
			dd($e->getMessage());
			$message = [
                'message' => 'Failed save delivery transaction !'
            ];
			return response()->json($message, 422);
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$data = DB::table('view_delivery_transaction')->where('id', $id)->first();
		$message = [
			'message' => 'Success',
			'data' => $data
		];
		return response()->json($message, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function showDetail($id)
	{
		$data = DB::table('view_delivery_transaction_detail')->where('delivery_transaction_id', $id)->get();
		$message = [
			'message' => 'Success',
			'data' => $data
		];
		return response()->json($message, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * upload evidence of delivery when items delivered to customer.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function uploadFile(Request $request)
	{
		// 1. Validate the request
		$validator = Validator::make(
			$request->all(),
			$this->rules()
		);

		if ($validator->fails()) {
			$message = [
                'errors' => $validator->errors()->toArray(),
                'message' => collect($validator->errors()->all())->implode('. '),
            ];
            return response()->json($message, 422);
        }

		// 2. Setting image file upload and initial ID Delivery Transaction
		$id = $request->input('id');
		$images = $request->file('image');
		$fileName = time().'.'.$images->getClientOriginalExtension();
		$pathImages = public_path()."/images";
		File::isDirectory($pathImages) or File::makeDirectory($pathImages, 0777, true, true);
		$images->move($pathImages, $fileName);

		// 3. Update the transaction status
		$delivery_transaction = DeliveryTransaction::find($id);
		$delivery_transaction->evidence_delivery_images = $fileName;
		$delivery_transaction->status = 1;
		$delivery_transaction->delivery_time = Carbon::now();
		$delivery_transaction->save();

		// 4. Sent email to admin when transaction delivered
		$emailUser = Auth::user()->email;
		$userId = Auth::user()->id;
        $getDataCourier = Courier::where('user_id', $userId)->first();
		$dataMail = [
			'sttb_number' => $delivery_transaction->sttb_number,
			'name' => $getDataCourier->name,
			'mail_from' => $emailUser,
			'name_from' => $getDataCourier->name,
			'mail_subject' => "Evidence of Delivery Transaction With STTB Number ".$delivery_transaction->sttb_number,
			'file_name' => url('/images')."/".$fileName,
			'email' => 'admin@admin.com'
		];

		// Dispatch to Job Email
		dispatch(new SendEmail($dataMail));

		$message = [
			'message' => 'Success upload evidence of delivery!'
		];
		return response()->json($message, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$service_vehicle = ServiceVehicle::findOrFail($id);
		$service_vehicle->delete();
		$message = [
			'message' => 'Success remove service vehicle!'
		];
		return response()->json($message, 200);
	}

	/**
	 * @Author: Adhitya Giva Muhammad
	 * @Date: 2019-11-13 21:08:53
	 * @Desc: Update status service vehicle
	 */
	public function updateStatusService(Request $request, $id)
	{
		$status = $request->input('status');
		$service_vehicle = ServiceVehicle::find($id);

		switch ($status) {
			case "0":
				$message_desc = "Service vehicle approved!";
				$service_vehicle->service_date = $request->input('service_date');
				$service_vehicle->status = 1;
				break;
			case "1":
				$message_desc = "Finish service vehicle!";
				$service_vehicle->finish_service_date = \date("Y-m-d H:i:s", \time());
				$service_vehicle->status = 2;
				break;
			default:
				$message_desc = "Failed get status";
				$service_vehicle->service_date = null;
				$service_vehicle->finish_service_date = null;
				$service_vehicle->status = 0;
				break;
		}

		$service_vehicle->save();

		$message = [
			'message' => $message_desc
		];
		return response()->json($message, 200);
    }

    /**
	 * @Author: Adhitya Giva Muhammad
	 * @Date: 2019-12-03 02:23:01
	 * @Desc:  Printout STTB/DO
	 */
    public function printSTTB($id)
    {
        $dataDetailTransaction = DB::table('view_delivery_transaction_detail')->where('delivery_transaction_id', $id)->get();
        $dataTransaction = DB::table('view_delivery_transaction')->where('id', $id)->first();
        $pdf = PDF::loadview(
            'print.print_sttb',
            [
                'data' => $dataTransaction,
                'detailTransaction' => $dataDetailTransaction
            ]
        );
        return $pdf->stream();
    }

    /**
	 * @Author: Adhitya Giva Muhammad
	 * @Date: 2019-12-03 02:23:01
	 * @Desc:  Printout STTB/DO
	 */
    public function printDO($id)
    {
        $dataDetailTransaction = DB::table('view_delivery_transaction_detail')->where('delivery_transaction_id', $id)->get();
        $dataTransaction = DB::table('view_delivery_transaction')->where('id', $id)->first();
        $pdf = PDF::loadview(
            'print.print_do',
            [
                'data' => $dataTransaction,
                'detailTransaction' => $dataDetailTransaction
            ]
        );
        return $pdf->stream();
    }

	/**
	 * @Author: Adhitya Giva Muhammad
	 * @Date: 2019-10-15 15:53:09
	 * @Desc:  Return datatable
	 */
	public function anyData()
	{
		// $data = ServiceVehicle::all();
		$data = DB::table('view_delivery_transaction')->get();
		$user = Auth::user();
		$userId = $user->id;
		$getCourierId = Courier::where("user_id", "=", $userId)->pluck("id")->first();
		if ($user->getRoleNames()[0] != "admin") {
			$data = DB::table('view_delivery_transaction')->where("courier_id", "=", $getCourierId)->get();
		}

		return Datatables::of($data)
				->addIndexColumn()
				->addColumn('action', function($row){
					$user = Auth::user();

					$btn = "";
					if (($row->status != 1) && ($user->hasRole('admin') || $user->hasRole('courier')) ) {
						$btn .=
						'<a href="javascript:void(0)" onclick="uploadEvidenceDelivery(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Upload Evidence Delivery" class="edit btn waves-effect waves-light btn-info">
							<i class="fa fas fa-upload"></i>
						</a> ';
					}


					if ($user->hasRole('admin') && $row->status != 1) {
						$btn .=
						'<a href="'.route("print.delivery-order-sttb", $row->id).'" target="_blank" data-toggle="tooltip" data-placement="top" title="Print STTB" class="delete btn waves-effect waves-light btn-success">
							<i class="fa fas fa-file-alt"></i>
						</a> ';

						$btn .=
						'<a href="'.route("print.delivery-order-do", $row->id).'" target="_blank" data-toggle="tooltip" data-placement="top" title="Print DO" class="delete btn waves-effect waves-light btn-warning">
							<i class="fa fas fa-file-alt"></i>
						</a> ';
					}

					$btn .=
					'<a href="javascript:void(0)" onclick="detailDeliveryOrder(\''.$row->id.'\')" data-toggle="tooltip" data-placement="top" title="Detail Delivery Order" class="delete btn waves-effect waves-light btn-primary">
						<i class="fa fas fa-list-alt"></i>
					</a> ';
					return $btn;
				})
				->rawColumns(['action'])
				->make(true);
	}

	public function rules()
	{
		return ['image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10000'];
	}
}
