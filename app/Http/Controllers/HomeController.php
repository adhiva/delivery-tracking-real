<?php

namespace App\Http\Controllers;


use App\Models\DeliveryTransaction;
use App\Models\ServiceVehicle;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    var $data = array();

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->data['transactionProcess'] = DeliveryTransaction::where('status', 0)
            ->get()
            ->count();
            
        $this->data['transactionSuccess'] = DeliveryTransaction::whereDate('created_at', Carbon::now())
            ->where('status', 1)
            ->get()
            ->count();

        $this->data['serviceVehicle'] = ServiceVehicle::where('status', '!=', '2')
            ->get()
            ->count();
        
        return view('home.dashboard', $this->data);
    }
}
