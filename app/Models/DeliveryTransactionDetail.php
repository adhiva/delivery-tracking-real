<?php

namespace App\Models;


class DeliveryTransactionDetail extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'delivery_transaction_detail';

    # Set fillable field in table
    protected $fillable = [
        'quantity',
        'delivery_transaction_id',
        'item_id'
    ];
}
