<?php

namespace App\Models;

class Items extends BaseModel
{
    # set table name according what you want
    protected $table = "items";

    # Set fillable field in table
    protected $fillable = [
        'name',
        'price',
        'quantity'
    ];

}
