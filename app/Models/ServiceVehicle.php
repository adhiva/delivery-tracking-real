<?php

namespace App\Models;

class ServiceVehicle extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_vehicle';

    # Set fillable field in table
    protected $fillable = [
        'complaint',
        'service_date',
        'status',
        'finish_service_date',
        'courier_id',
        'vehicle_id'
    ];
}
