<?php

namespace App\Models;

class DeliveryTransaction extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'delivery_transaction';

    # Set fillable field in table
    protected $fillable = [
        'sttb_number',
        'do_number',
        'status',
        'delivery_time',
        'total_harga',
        'client_company_id',
        'vehicle_id',
        'courier_id'
    ];
}
