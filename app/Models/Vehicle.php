<?php

namespace App\Models;

class Vehicle extends BaseModel
{
    # set table name according what you want
    protected $table = "vehicle";

    # Set fillable field in table
    protected $fillable = [
        'number_plate',
        'merk',
        'type',
        'capacity',
        'pcs',
        'service_schedule_monthly'
    ];

}
