<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;

trait EventUpdater
{
    protected static function boot()
    {
        parent::boot();

        /*
         * During a model create Eloquent will also update the updated_at field
         * so need to have the updated_by field here as well
         * */
        static::creating(function ($model) {
            $model->created_by = Auth::check() ? Auth::user()->name : 'system';
            $model->updated_by = Auth::check() ? Auth::user()->name : 'system';
        });

        static::updating(function ($model) {
            $model->updated_by = Auth::check() ? Auth::user()->name : 'system';
        });

        /*
         * Deleting a model is slightly different than creating or updating. For
         * deletes we need to save the model first with the deleted_by field
         * */
        static::deleting(function ($model) {
            $model->deleted_by = Auth::check() ? Auth::user()->name : 'system';
            $model->save();
        });
    }
}
