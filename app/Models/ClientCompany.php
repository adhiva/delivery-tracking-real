<?php

namespace App\Models;


class ClientCompany extends BaseModel
{
    # set table name according what you want
    protected $table = "client_company";

    # Set fillable field in table
    protected $fillable = [
        'name',
        'address',
        'contact'
    ];
}
