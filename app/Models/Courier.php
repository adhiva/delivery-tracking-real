<?php

namespace App\Models;

class Courier extends BaseModel
{
    # set table name according what you want
    protected $table = "courier";

    # Set fillable field in table
    protected $fillable = [
        'name',
        'number_sim',
        'number_identity',
        'gender',
        'date_of_birth',
        'address'
    ];

}
