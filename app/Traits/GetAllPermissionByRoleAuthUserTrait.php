<?php
namespace App\Traits;

use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

trait GetPermissionByRoleAuthUser
{
	public function getPermissionByUser(){
		$userRoles = Auth::user()->roles()->pluck('name')[0];
        $userPermission = Role::findByName($userRoles)->permissions()->pluck('name');
        return $userPermission;
	}
}
