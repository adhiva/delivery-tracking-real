<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait AutoNumberTransaction
{
	public function getTransactionNumber($type = ""){
		$field = (\strtoupper($type) == "DO") ? "do_number" : "sttb_number";
		$lastNumber = DB::table('view_delivery_transaction')->select(DB::raw('MAX(RIGHT('.$field.', 4)) as max_id'))->orderBy($field, 'desc')->first();
		$max_id = $lastNumber->max_id;
		$sort_num = (int) substr($max_id, 1, 4);
		$sort_num++;
		$new_code = sprintf("%04s", $sort_num);
		$prefix = (\strtoupper($type) == "DO") ? "FR-DLV-" : "FR-STTB-";
		$new_code = $prefix . $new_code;
		return $new_code;
	}
}
