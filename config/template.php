<?php
	return [
		/*
		|--------------------------------------------------------------------------
		| Menu Items
		|--------------------------------------------------------------------------
		|
		| Specify your menu items to display in the left sidebar. Each menu item
		| should have a text and and a URL. You can also specify an icon from
		| Material Design. A string instead of an array represents a header in sidebar
		| layout. The 'can' is a filter on Laravel's built in Gate functionality.
		|
		*/

		'menu' => [
            [
                'title' => "Dashboard",
                'icon' => "mdi-home-outline",
                'url' => "/",
                'modulCode' => "DASHBOARD",
            ],
            [
                'title' => "Delivery",
                'icon' => "mdi-truck-delivery",
                'url' => "/delivery-order",
                'modulCode' => "DELIVERY_ORDER",
            ],
            [
                'title' => "User Manager",
                'icon' => "mdi-account-settings-variant",
                'modulCode' => "USER_MANAGEMENT",
                'subMenu' => [
                    [
                        'url' => "/users",
                        'title' => "Users",
                        'modulCode' => "USERS",
                    ],
                    [
                        'url' => "/roles",
                        'title' => "Roles And Permissions",
                        'modulCode' => "ROLES_AND_PERMISSIONS",
                    ],
                ]
            ],
            [
                'title' => "Data Master",
                'icon' => "mdi-view-list",
                'modulCode' => "MASTER",
                'subMenu' => [
                    [
                        'url' => "/vehicle",
                        'title' => "List Vehicle",
                        'modulCode' => "LIST_VEHICLE",
                    ],
                    [
                        'url' => "/courier",
                        'title' => "List Courier",
                        'modulCode' => "LIST_COURIER",
                    ],
                    [
                        'url' => "/items",
                        'title' => "List Items",
                        'modulCode' => "LIST_ITEMS",
                    ],
                    [
                        'url' => "/client-company",
                        'title' => "List Client Company",
                        'modulCode' => "LIST_CLIENT_COMPANY",
                    ]
                ]
            ],
            [
                'title' => "Service",
                'icon' => "mdi-wrench",
                'modulCode' => "SERVICE",
                'url' => "/service",
            ],
		]
	];