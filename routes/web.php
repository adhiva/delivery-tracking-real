<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/test-email', 'HomeController@testMail')->name('mail.test');
Route::group(['namespace' => 'UserManagement'], function () {

    Route::group(['prefix' => 'users',], function () {
        Route::get('/', 'UserController@index')->name('show.user');
        Route::post('/', 'UserController@store')->name('store.user');
        Route::get('/get/{id}', 'UserController@show')->name('get.user');
        Route::put('/update/{id}', 'UserController@update')->name('update.user');
        Route::get('/datatable', 'UserController@anyData')->name('show.user-datatable');
        Route::post('/user-role', 'UserController@giveRoleToUser')->name('store.user-role');
        Route::delete('delete/{id}', 'UserController@destroy')->name('destroy.user');
    });

    Route::group(['prefix' => 'roles',], function () {
        Route::get('/', 'RoleAndPermissionController@index')->name('show.role');
        Route::post('/', 'RoleAndPermissionController@store')->name('store.role');
        Route::get('/list', 'RoleAndPermissionController@list')->name('list.role');
        Route::get('/datatable', 'RoleAndPermissionController@anyData')->name('show.role-datatable');
        Route::put('/{id}', 'RoleAndPermissionController@update')->name('update.role');
        Route::delete('/{id}', 'RoleAndPermissionController@destroy')->name('destroy.role');
    });

    Route::group(['prefix' => 'permission',], function () {
        Route::get('/', 'RoleAndPermissionController@getPermission')->name('list.permission');
        Route::get('/role/{id}', 'RoleAndPermissionController@getPermissionByRole')->name('list.permissions-by-role');
        Route::post('/role', 'RoleAndPermissionController@givePermissionToRole')->name('store.permission-role');
        Route::put('/revoke/{id}', 'RoleAndPermissionController@revokePermissionFromRole')->name('revoke.permission');
    });
});

Route::group(['namespace' => 'Master'], function () {
    Route::group(['prefix' => 'vehicle',], function () {
        Route::get('/', 'VehicleController@index')->name('show.vehicle');
        Route::post('/', 'VehicleController@store')->name('store.vehicle');
        Route::get('/get/{id}', 'VehicleController@show')->name('get.vehicle');
        Route::get('/datatable', 'VehicleController@anyData')->name('list.vehicle-datatable');
        Route::put('/{id}', 'VehicleController@update')->name('update.vehicle');
        Route::delete('/{id}', 'VehicleController@destroy')->name('destroy.vehicle');
        Route::get('/search', 'VehicleController@loadData')->name('select2.vehicle');
    });

    Route::group(['prefix' => 'courier',], function () {
        Route::get('/', 'CourierController@index')->name('show.courier');
        Route::get('/get/{id}', 'CourierController@show')->name('get.courier');
        Route::post('/', 'CourierController@store')->name('store.courier');
        Route::get('/datatable', 'CourierController@anyData')->name('list.courier-datatable');
        Route::put('/{id}', 'CourierController@update')->name('update.courier');
        Route::delete('/{id}', 'CourierController@destroy')->name('destroy.courier');
        Route::get('/user/search', 'CourierController@loadDataCourierAsUser')->name('select2.courier-user');
        Route::get('/search', 'CourierController@loadData')->name('select2.courier');
        Route::post('/user', 'CourierController@addUserToCourier')->name('store.user-courier');
    });

    Route::group(['prefix' => 'items',], function () {
        Route::get('/', 'ItemController@index')->name('show.item');
        Route::get('/get/{id}', 'ItemController@show')->name('get.item');
        Route::post('/', 'ItemController@store')->name('store.item');
        Route::get('/datatable', 'ItemController@anyData')->name('list.item-datatable');
        Route::put('/{id}', 'ItemController@update')->name('update.item');
        Route::delete('/{id}', 'ItemController@destroy')->name('destroy.item');
        Route::get('/search', 'ItemController@loadData')->name('select2.item');
    });

    Route::group(['prefix' => 'client-company',], function () {
        Route::get('/', 'ClientCompanyController@index')->name('show.client');
        Route::get('/get/{id}', 'ClientCompanyController@show')->name('get.client');
        Route::post('/', 'ClientCompanyController@store')->name('store.client');
        Route::get('/datatable', 'ClientCompanyController@anyData')->name('list.client-datatable');
        Route::put('/{id}', 'ClientCompanyController@update')->name('update.client');
        Route::delete('/{id}', 'ClientCompanyController@destroy')->name('destroy.client');
        Route::get('/search', 'ClientCompanyController@loadData')->name('select2.client');
    });

    Route::group(['prefix' => 'service',], function () {
        Route::get('/', 'ServiceVehicleController@index')->name('show.service');
        Route::put('/update-status/{id}', 'ServiceVehicleController@updateStatusService')->name('update.service-status');
        Route::get('/get/{id}', 'ServiceVehicleController@show')->name('get.service');
        Route::post('/', 'ServiceVehicleController@store')->name('store.service');
        Route::get('/datatable', 'ServiceVehicleController@anyData')->name('list.service-datatable');
        Route::put('/{id}', 'ServiceVehicleController@update')->name('update.service');
        Route::delete('/{id}', 'ServiceVehicleController@destroy')->name('destroy.service');
    });
});

Route::group(['namespace' => 'Transaction'], function () {
    Route::group(['prefix' => 'delivery-order',], function () {
        Route::get('/', 'DeliveryOrderController@index')->name('show.delivery-order');
        Route::post('/', 'DeliveryOrderController@store')->name('store.delivery-order');
        Route::get('/get/{id}', 'DeliveryOrderController@show')->name('get.delivery-order');
        Route::get('/get/detail/{id}', 'DeliveryOrderController@showDetail')->name('get.detail-delivery-order');
        Route::get('/datatable', 'DeliveryOrderController@anyData')->name('list.delivery-order-datatable');
        Route::put('/{id}', 'DeliveryOrderController@update')->name('update.delivery-order');
        Route::post('/upload_file', 'DeliveryOrderController@uploadFile')->name('upload.delivery-order');
        Route::delete('/{id}', 'DeliveryOrderController@destroy')->name('destroy.delivery-order');
        Route::get('/search', 'DeliveryOrderController@loadData')->name('select2.delivery-order');
        Route::get('/print/sttb/{id}', 'DeliveryOrderController@printSTTB')->name('print.delivery-order-sttb');
        Route::get('/print/do/{id}', 'DeliveryOrderController@printDO')->name('print.delivery-order-do');
    });
});
