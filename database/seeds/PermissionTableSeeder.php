<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = config('template.menu');
        foreach ($menus as $menu) {
            if (isset($menu['subMenu'])) {
                foreach ($menu['subMenu'] as $subMenu) {
                    Permission::create(
                        [
                            'name' => $subMenu['modulCode']
                        ]
                    );
                }
            }

            Permission::create(
                [
                    'name' => $menu['modulCode']
                ]
            );
        }
    }
}
