<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryOrderDetailView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            "CREATE 
                VIEW `view_delivery_transaction_detail` AS 
                SELECT
                    `delivery_transaction_detail`.`id` AS `id`,
                    `delivery_transaction_detail`.`quantity` AS `quantity`,
                    `delivery_transaction_detail`.`created_by` AS `created_by`,
                    `delivery_transaction_detail`.`updated_by` AS `updated_by`,
                    `delivery_transaction_detail`.`deleted_by` AS `deleted_by`,
                    `delivery_transaction_detail`.`deleted_at` AS `deleted_at`,
                    `delivery_transaction_detail`.`created_at` AS `created_at`,
                    `delivery_transaction_detail`.`updated_at` AS `updated_at`,
                    `delivery_transaction_detail`.`delivery_transaction_id` AS `delivery_transaction_id`,
                    `delivery_transaction_detail`.`item_id` AS `item_id`,
                    `items`.`name` AS `item_name`,
                    `items`.`price` AS `price`
                FROM
                    (
                        `delivery_transaction_detail`
                        LEFT JOIN `items` ON (
                            (
                                `items`.`id` = `delivery_transaction_detail`.`item_id`
                            )
                        )
                    ) ;
            "
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_order_detail_view');
    }
}
