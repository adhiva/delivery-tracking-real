<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTriggerUpdateItemQuantity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE TRIGGER `UPDATE_QTY_ITEMS` AFTER INSERT ON `delivery_transaction_detail`
            FOR EACH ROW BEGIN
            UPDATE items
                SET quantity = quantity - NEW.quantity
            WHERE id = NEW.item_id;
            END;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER `UPDATE_ITEMS_QTY`;");
    }
}
