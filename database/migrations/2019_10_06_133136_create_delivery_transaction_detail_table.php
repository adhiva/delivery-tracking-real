<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryTransactionDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_transaction_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('quantity')->default(0)->nullable();
            $table->string('created_by',100);
            $table->string('updated_by',100);
            $table->string('deleted_by',100)->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->bigInteger('delivery_transaction_id')->default(0)->nullable(false);
            $table->bigInteger('item_id')->default(0)->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_transaction_detail');
    }
}
