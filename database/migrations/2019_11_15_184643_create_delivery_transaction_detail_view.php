<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryTransactionDetailView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            "CREATE 
            VIEW `view_delivery_transaction` AS 
            SELECT
                delivery_transaction.*, courier.`name` as courier_name,
                courier.`number_identity`,
                client_company.`name` AS client_company_name,
                client_company.address,
                client_company.contact,
                vehicle.merk,
                vehicle.type
            FROM
                delivery_transaction
            LEFT JOIN client_company ON client_company.id = delivery_transaction.client_company_id
            LEFT JOIN courier ON courier.id = delivery_transaction.courier_id
            LEFT JOIN vehicle ON vehicle.id = delivery_transaction.vehicle_id
            WHERE
                delivery_transaction.deleted_at IS NULL;
            "
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_delivery_transaction");
    }
}
