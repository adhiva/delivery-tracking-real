<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_vehicle', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('complaint')->nullable();
            $table->date('service_date')->nullable();
            $table->integer('status')->nullable(false)->default(0)->comment("0 : Process to Service, 1 : Accept Service, 2: Finished Service");
            $table->date('finish_service_date')->nullable();
            $table->string('created_by',100);
            $table->string('updated_by',100);
            $table->string('deleted_by',100)->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->bigInteger('courier_id')->default(0);
            $table->bigInteger('vehicle_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_vehicle');
    }
}
