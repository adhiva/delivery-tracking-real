<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateServiceVehicleView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            "CREATE VIEW view_service_vehicle AS SELECT
            `service_vehicle`.`id` AS `id`,
            `service_vehicle`.`complaint` AS `complaint`,
            `service_vehicle`.`service_date` AS `service_date`,
            `service_vehicle`.`status` AS `status`,
            `service_vehicle`.`finish_service_date` AS `finish_service_date`,
            `service_vehicle`.`created_by` AS `created_by`,
            `service_vehicle`.`updated_by` AS `updated_by`,
            `service_vehicle`.`deleted_by` AS `deleted_by`,
            `service_vehicle`.`deleted_at` AS `deleted_at`,
            `service_vehicle`.`created_at` AS `created_at`,
            `service_vehicle`.`updated_at` AS `updated_at`,
            `service_vehicle`.`courier_id` AS `courier_id`,
            `service_vehicle`.`vehicle_id` AS `vehicle_id`,
            `vehicle`.`type` AS `type`,
            `vehicle`.`merk` AS `merk`,
            `courier`.`name` AS `courier_name`
        FROM
            (
                (
                    `service_vehicle`
                    LEFT JOIN `vehicle` ON (
                        (
                            `vehicle`.`id` = `service_vehicle`.`vehicle_id`
                        )
                    )
                )
                LEFT JOIN `courier` ON (
                    (
                        `courier`.`id` = `service_vehicle`.`courier_id`
                    )
                )
            )
            WHERE service_vehicle.deleted_at IS NULL AND courier.deleted_at IS NULL"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_service_vehicle");
    }
}
