<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKendaraanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('number_plate',50);
            $table->string('merk',50);
            $table->string('type',50);
            $table->integer('capacity');
            $table->string('pcs')->default("karton")->comment("Satuan barang yang dikirim");
            $table->date('service_schedule_monthly')->nullable();
            $table->string('created_by',100);
            $table->string('updated_by',100);
            $table->string('deleted_by',100)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle');
    }
}
