<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nip',25);
            $table->string('name',100);
            $table->date('birth_date');
            $table->text('address');
            $table->string('contact',15);
            $table->string('created_by',100);
            $table->string('updated_by',100);
            $table->string('deleted_by',100)->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->bigInteger('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_information');
    }
}
