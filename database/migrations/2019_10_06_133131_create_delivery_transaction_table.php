<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sttb_number',100)->nullable()->comment("DO Number ex: FR-STTB-<IncrementsNumber>");
            $table->string('do_number',100)->nullable()->comment("DO Number ex: FR-DO-<IncrementsNumber>");
            $table->char('status', 1)->default('0')->nullable()->comment("0 : Proses, 1 : Terkirim");
            $table->dateTime('delivery_time')->nullable();
            $table->float('total_harga')->default('0')->nullable();
            $table->text('evidence_delivery_images')->nullable()->comment("Bukti Delivery Order When Driver Finish It");
            $table->string('created_by',100);
            $table->string('updated_by',100);
            $table->string('deleted_by',100)->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->bigInteger('client_company_id');
            $table->bigInteger('vehicle_id');
            $table->bigInteger('courier_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_transaction');
    }
}
