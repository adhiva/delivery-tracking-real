@extends('app')

<!-- Buat define title per page, misalkan gak di define pake config template => title -->
@section('title', 'Home')

@section('body')
    <!-- NAVBAR -->
    @include('partial.header')
    @yield('header')

    <section>
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
                <!-- Sidebar scroll-->
                <div class="scroll-sidebar">
                    <!-- User profile -->
                    <div class="user-profile" style="background: url({{ asset('assets/images/background/user-info.jpg') }}) no-repeat;">
                        <!-- User profile image -->
                        <div class="profile-img"> <img src="{{ asset('assets/images/users/logo.jpeg') }}" alt="user" /> </div>
                        <!-- User profile text-->
                        <div class="profile-text"> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown"
                            role="button" aria-haspopup="true" aria-expanded="true">{{ Auth::user()->name }} - {{ Auth::user()->getRoleNames()->first() }}</a>
                            <div class="dropdown-menu animated flipInY">
                                <a href="#" class="dropdown-item">
                                    <i class="ti-user"></i>
                                    My Profile
                                </a>
                                <div class="dropdown-divider"></div>
                                 <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i>
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- End User profile text-->
                    <!-- Sidebar navigation-->
                    <nav class="sidebar-nav">
                        <ul id="sidebarnav">
                            @each('partial.left_sidebar', config('template.menu'), 'menu')
                        </ul>
                    </nav>
                    <!-- End Sidebar navigation -->
                </div>
                <!-- End Sidebar scroll-->
                <!-- Bottom points-->
            </aside>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->

    </section>

    <section class="content">
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">@yield('headerPages', config('app.header_pages', ''))</h3>
                        @isset($data['breadcrumb'])
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Form Basic</li>
                            </ol>
                        @endisset
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                @yield('content_body')

            </div>
        </div>
    </section>

    <!-- NAVBAR -->
    @include('partial.footer')
    @yield('footer')
@endsection
