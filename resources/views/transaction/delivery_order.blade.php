@extends('home')

@section('headerPages', 'List Delivery Transaction')

@section('content_body')
    <div class="card">
        <div class="card-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="mytooltip nav-link active" data-toggle="tab" href="#listDeliveryTransaction" role="tab">
                        <span>
                            <i data-toggle="tooltip" data-placement="top" title="Delivery Transaction List" class="fa fas fa-list-alt"></i>
                        </span>
                    </a>
                </li>
                @role('admin')
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#addDeliveryTransaction" role="tab">
                        <span>
                            <i data-toggle="tooltip" data-placement="top" title="Add Delivery Transaction" class="fa fas fa-plus"></i>
                        </span>
                    </a>
                </li>
                @endrole
            </ul>
            <!-- Tab panes -->
            <div class="tab-content tabcontent-border">
                <div class="tab-pane active" id="listDeliveryTransaction" role="tabpanel">
                    <div class="p-20">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <th>No</th>
                                    <th>Customer</th>
                                    <th>Courier</th>
                                    <th>STTB Number</th>
                                    <th>DO Number</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                {{-- Form Add --}}
                <div class="tab-pane  p-20" id="addDeliveryTransaction" role="tabpanel">
                    <form class="form-horizontal m-t-40" id="formAddDeliveryTransaction">
                        <div class="form-group">
                            <label>Courier</label>
                            <select name="courier_id" class="select2-courier" style="width: 100%"></select>
                        </div>
                        <div class="form-group">
                            <label>Client Company</label>
                            <select name="client_company_id" class="select2-client" style="width: 100%"></select>
                        </div>
                        <div class="form-group">
                            <label>Vehicle</label>
                            <select name="vehicle_id" class="select2-vehicle" style="width: 100%"></select>
                        </div>

                        <h4 class="card-title">Item Barang</h4>
                        <div id="item_fields"></div>
                        <div class="row">
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <select name="item_id[]" class="select2-item" style="width: 100%"></select>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="priceClass-1" name="harga[]" value="" placeholder="Price" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control qty-check" id="qtyClass-1" name="quantity[]" value="" placeholder="Quantity">
                                        <div class="input-group-append">
                                            <button class="btn btn-success" type="button" onclick="education_fields();"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Total Harga</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="total_harga" placeholder="Total Harga" disabled>
                                <input type="hidden" id="sum_qty">
                                <div class="input-group-append">
                                    <button class="btn btn-success" type="button" onclick="getTotalHarga();"><i class="fa fas fa-calculator"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                            <button type="button" class="btn btn-inverse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- Start Modal Here --}}
        {{-- Detail DeliveryTransactions Modal --}}
        <div id="showDetailDeliveryModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            Delivery <span class="title-delivery-number"></span>
                        </h4>
                        <button type="button" class="close align-right" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="formUpdateDeliveryTransaction">
                            <div class="form-group">
                                <label>Customer</label>
                                <input type="text" id="show_customer" disabled class="form-control" placeholder="Customer">
                            </div>

                            <div class="form-group">
                                <label>Courier</label>
                                <input type="text" id="show_courier" disabled class="form-control" placeholder="Courier">
                            </div>

                            <div class="form-group">
                                <label>Vehicle</label>
                                <input type="text" id="show_vehicle" disabled class="form-control" placeholder="Vehicle">
                            </div>

                            <div class="form-group">
                                <label>DO Number</label>
                                <input type="text" id="show_do_number" disabled class="form-control" placeholder="DO Number">
                            </div>

                            <div class="form-group">
                                <label>STTB Number</label>
                                <input type="text" id="show_sttb_number" disabled class="form-control" placeholder="STTB Number">
                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <input type="text" disabled id="show_status" class="form-control mdate" placeholder="Status">
                            </div>

                            <div class="form-group">
                                <label>Total Price</label>
                                <input type="text" disabled id="show_total_price" class="form-control mdate" placeholder="Total Price">
                            </div>

                            <div class="form-group evidence-container" style="display: none;">
                                <label>Evidence Images</label>
                                <div class="col-lg-3 col-md-6">
                                    <div class="card">
                                        <img class="card-img-top img-fluid evidence-img" src="../assets/images/big/img1.jpg" alt="Card image cap">
                                    </div>
                                </div>
                            </div>

                            <table class="table table-hover" id="table_detail_transaction">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- Upload Evidence Modal --}}
        <div id="uploadEvidenceModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            Upload Evidence <span class="title-evidence"></span>
                        </h4>
                        <button type="button" class="close align-right" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="uploadEvidenceForm" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Customer</label>
                                <input type="text" id="upload_customer" disabled class="form-control" placeholder="Customer">
                            </div>

                            <div class="form-group">
                                <label>Courier</label>
                                <input type="text" id="upload_courier" disabled class="form-control" placeholder="Courier">
                            </div>

                            <div class="form-group">
                                <label>Vehicle</label>
                                <input type="text" id="upload_vehicle" disabled class="form-control" placeholder="Vehicle">
                            </div>

                            <div class="form-group">
                                <label>DO Number</label>
                                <input type="text" id="upload_do_number" disabled class="form-control" placeholder="DO Number">
                            </div>

                            <div class="form-group">
                                <label>STTB Number</label>
                                <input type="text" id="upload_sttb_number" disabled class="form-control" placeholder="STTB Number">
                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <input type="text" disabled id="upload_status" class="form-control mdate" placeholder="Status">
                            </div>

                            <div class="form-group">
                                <label>Total Price</label>
                                <input type="text" disabled id="upload_total_price" class="form-control mdate" placeholder="Total Price">
                            </div>

                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label form-control" for="customFile">Choose file</label>
                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="button" id="uploadEvidenceButton" class="btn btn-success">Upload Evidence</button>
                                                <button type="button" class="btn btn-inverse" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('templateCSS')
    <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />

    {{-- Sweet Alert --}}
    <link href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">

    {{-- Material Date Time Picker --}}
    <link href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
    <style>
        .modal-dialog{
            max-width: 700px !important;
        }
    </style>
@endsection

@section('templateJS')
    <script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    {{-- Pretty tooltips --}}
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?lang=css&amp;skin=default"></script>
    <!-- end - This is for export functionality only -->

    {{-- Select2 --}}
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}" type="text/javascript"></script>

    {{-- Sweet Alert --}}
    <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js') }}"></script>

    <!-- Material Design Bootstrap Datepicker -->
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    <script>
        $( document ).ready(function() {
            // datepicker material design
            $('.mdate').bootstrapMaterialDatePicker(
                {
                    weekStart: 0,
                    time: false
                }
            );

            // Init Select2
            $(".select2-vehicle").select2({
                placeholder: 'Choose vehicle',
                ajax: {
                    url: '{{ route("select2.vehicle") }}' + '?is_do=1',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.type,
                                    id: item.id,
                                    capacity: item.capacity
                                }
                            })
                        };
                    },
                    cache: true
                }
            });

            // Init Select2 Client
            $(".select2-client").select2({
                placeholder: 'Choose client',
                ajax: {
                    url: '{{ route("select2.client") }}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    cache: true
                }
            });


            // Init Select2 Courier
            $(".select2-courier").select2({
                placeholder: 'Choose courier',
                ajax: {
                    url: '{{ route("select2.courier") }}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    cache: true
                }
            });

            // Init Select2 Items
            $(".select2-item").select2({
                placeholder: 'Choose item',
                ajax: {
                    url: '{{ route("select2.item") }}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id,
                                    quantity : item.quantity,
                                    price : item.price,
                                    room : 0
                                }
                            })
                        };
                    },
                    cache: true
                }
            }).on('change', function (e) {
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();
                let data = $(this).select2('data')[0];
                $('#priceClass-1').val(data.price)
                $('#qtyClass-1').attr("data-qty", data.quantity);
            });

            var table = $('#myTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('list.delivery-order-datatable') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'client_company_name', name: 'client_company_name'},
                    {data: 'courier_name', name: 'courier_name'},
                    {data: 'sttb_number', name: 'sttb_number'},
                    {data: 'do_number', name: 'do_number'},
                    {
                        data: 'status',
                        render: function ( data, type, row ) {

                            dataStatus = mappingStatus(data)

                            colorStatus = dataStatus['colorStatus']
                            statusDesc = dataStatus['status']

                            let labelStatus = "<span class='label label-"+ colorStatus +"'>"+ statusDesc +"</span> ";
                            return labelStatus;
                        }
                    },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $('#formAddDeliveryTransaction').submit(function (e) {
                e.preventDefault();
                 // Find disabled inputs, and remove the "disabled" attribute
                var disabled = $(this).find(':input:disabled').removeAttr('disabled');
                let dataVehicle = $('.select2-vehicle').select2('data')[0];
                let capacityVehicle = dataVehicle.capacity;
                let totalQty = $('#sum_qty').val();
                if (totalQty > capacityVehicle) {
                    swal("Warning ! ", "Over limit quantity for vehicle, your vehicle capacity :  "+ capacityVehicle +" !", "warning")
                    return false;
                }
                // serialize the form
                data = $(this).serialize();

                // re-disabled the set of inputs that you previously enabled
                disabled.attr('disabled','disabled');


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });

                $.ajax({
                    type: "POST",
                    url: "{{ route('store.delivery-order') }}",
                    data: data,
                    dataType: "JSON",
                    success: function (response) {
                        swal("Success!", response.message, "success")
                        document.getElementById("formAddDeliveryTransaction").reset();
                        $('#myTable').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error ! ", "Internal Server Error !", "error")
                    }
                });
            });

        });
        var room = 1;

        function removeDeliveryTransaction(serviceVehicleId) {
            let url = '{{ route("destroy.delivery-order", ":id") }}';
            url = url.replace(':id', serviceVehicleId);

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                     $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        dataType: "JSON",
                        success: function (response) {
                            $('#myTable').DataTable().ajax.reload();
                            Swal.fire(
                                'Deleted!',
                                response.message,
                                'success'
                            )
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            Swal.fire(
                                'Error',
                                'Internal Server Error!',
                                'warning'
                            )
                        }
                    });

                } else {
                    Swal.fire(
                        'Safe!',
                        'Your data has been safe.',
                        'success'
                    )
                }
            })
        }

        function uploadEvidenceDelivery(deliveryTransactionId) {
            let deliveryTransactionData = getData(deliveryTransactionId)

            $('#uploadEvidenceModal').modal('show');

            $('#upload_customer').val(deliveryTransactionData.client_company_name);
            $('#upload_courier').val(deliveryTransactionData.courier_name);
            $('#upload_vehicle').val(deliveryTransactionData.merk + " " + deliveryTransactionData.type);
            $('#upload_do_number').val(deliveryTransactionData.do_number);
            $('#upload_sttb_number').val(deliveryTransactionData.sttb_number);
            $('#upload_status').val(mappingStatus(deliveryTransactionData.status));
            $('#upload_total_price').val(deliveryTransactionData.total_harga);

            $('#uploadEvidenceButton').off();
            $('#uploadEvidenceButton').click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                let url = '{{ route("upload.delivery-order") }}';

                var fd = new FormData();
                var files = $('#customFile')[0].files[0];
                fd.append('image',files);
                fd.append('id', deliveryTransactionId);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });

                $.ajax({
                    type: "POST",
                    url: url,
                    data: fd,
                    dataType: "JSON",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        swal("Success!", response.message, "success")
                        $('#myTable').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        let message = "Internal Server Error!";
                        if (typeof xhr.responseJSON.message !== 'undefined') {
                            message = xhr.responseJSON.message
                        }

                        swal("Error ! ", message, "error")
                    }
                });
            });
        }

        // Getting data according page
        function getData(deliveryTransactionId) {
            let url = '{{ route("get.delivery-order", ":id") }}';
            url = url.replace(':id', deliveryTransactionId);

            let clientCompanies = "";
            $.ajax({
                type: "GET",
                url: url,
                dataType: "JSON",
                async: false,
                success: function (response) {
                    deliveryTransactions = response.data;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error ! ", "Internal Server Error !", "error")
                }
            });

            return deliveryTransactions;
        }

        // Get detail transaction
        function getDetailTransaction(deliveryTransactionId) {
            let url = '{{ route("get.detail-delivery-order", ":id") }}';
            url = url.replace(':id', deliveryTransactionId);

            $.ajax({
                type: "GET",
                url: url,
                dataType: "JSON",
                async: false,
                success: function (response) {
                    detailDeliveryTransactions = response.data;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error ! ", "Internal Server Error !", "error")
                }
            });

            return detailDeliveryTransactions;
        }

        // Show Detail Delivery Order
        function detailDeliveryOrder(deliveryTransactionId) {
            let deliveryTransactionData = getData(deliveryTransactionId)

            $('#showDetailDeliveryModal').modal('show');

            $('#show_customer').val(deliveryTransactionData.client_company_name);
            $('#show_courier').val(deliveryTransactionData.courier_name);
            $('#show_vehicle').val(deliveryTransactionData.merk + " " + deliveryTransactionData.type);
            $('#show_do_number').val(deliveryTransactionData.do_number);
            $('#show_sttb_number').val(deliveryTransactionData.sttb_number);
            $('#show_status').val(mappingStatus(deliveryTransactionData.status));
            $('#show_total_price').val(deliveryTransactionData.total_harga);
            if (deliveryTransactionData.evidence_delivery_images != "" && deliveryTransactionData.evidence_delivery_images != null) {
                $('.evidence-container').show();
                $('.evidence-img').attr("src", "{{ asset('images') }}/" + deliveryTransactionData.evidence_delivery_images);
            }

            $('#table_detail_transaction tr:gt(0)').remove()

            // Get Detail Transaction
            let detailDeliveryTransactions = getDetailTransaction(deliveryTransactionId)
            let bodyTableDetail = "";

            $.each(detailDeliveryTransactions, function (key, value) {
                bodyTableDetail += "<tr>"
                bodyTableDetail += "<td>"+ (parseInt(key+1)) +"</td>"
                bodyTableDetail += "<td>"+ value.item_name +"</td>"
                bodyTableDetail += "<td>"+ value.price +"</td>"
                bodyTableDetail += "<td>"+ value.quantity +"</td>"
                bodyTableDetail += "</tr>"
            });

            $('#table_detail_transaction tbody').append(bodyTableDetail)
        }

        function mappingStatus(status) {
            let mapStatus = [];
            switch (status) {
                case "0":
                    mapStatus['status'] = "On Process"
                    mapStatus['colorStatus'] = "info"
                    break;
                case "1":
                    mapStatus['status'] = "Delivered To Company"
                    mapStatus['colorStatus'] = "primary"
                    break;
                case "2":
                    mapStatus['status'] = "Finish"
                    mapStatus['colorStatus'] = "success"
                    break;
                default:
                    mapStatus['status'] = "Not Detected"
                    mapStatus['colorStatus'] = "warning"
                    break;
            }

            return mapStatus
        }

        function education_fields() {
            room++;

            // Re define the select2-item
            setTimeout(function(){
                $(".select2-item").select2({
                    placeholder: 'Choose items',
                    ajax: {
                        url: '{{ route("select2.item") }}',
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
                            return {
                                results:  $.map(data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id,
                                        quantity : item.quantity,
                                        price : item.price,
                                        room : room
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                }).on('change', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    let data = $(this).select2('data')[0];
                    let yourDiv = $(this).find('.form-group')
                    let closestDiv = $(this).closest('div');
                    let classes = $(this).parent().closest('div').attr('class').split(' ');
                    let clasessItems = classes[1].split('-')
                    let getRoom = clasessItems[1];

                    $('#priceClass-' + getRoom).val(data.price)
                    $('#qtyClass-' + getRoom).attr("data-qty", data.quantity);
                });
            }, 100);

            setTimeout(function(){
                $('.qty-check-'+ room).keyup(function (e) {
                    e.preventDefault()
                    let sum_qty = $(this).data('qty')
                    let input_qty = $(this).val()

                    if (input_qty > sum_qty) {
                        swal("Quantity over limit!", "Not enough quantity! quantity available : " + sum_qty ,"warning")
                        $(this).val("")
                    }
                });
            }, 100);

            var objTo = document.getElementById('item_fields')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group-"+ room +" removeclass" + room);
            var rdiv = 'removeclass' + room;
            divtest.innerHTML =
                '<div class="row">' +
                    '<div class="col-sm-3 nopadding">' +
                        '<div class="form-group selectItems-'+ room +'">' +
                            '<select name="item_id[]" class="select2-item" style="width: 100%"></select>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-sm-3 nopadding">' +
                        '<div class="form-group">' +
                            '<input type="text" class="form-control" id="priceClass-'+ room +'" name="harga[]" value="" placeholder="Price" disabled>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-sm-3 nopadding">' +
                        '<div class="form-group">' +
                            '<div class="input-group"> ' +
                                '<input type="text" class="form-control qty-check-'+ room +'" id="qtyClass-'+ room +'" name="quantity[]" value="" placeholder="Quantity">'+
                                '<div class="input-group-append"> ' +
                                    '<button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"> ' +
                                        '<i class="fa fa-minus"></i> ' +
                                    '</button>' +
                                '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="clear"></div>' +
                '</row>';

            objTo.appendChild(divtest)
        }

        function remove_education_fields(rid) {
            $('.removeclass' + rid).remove();
        }

        function getTotalHarga() {
            totalHargas = $("input[name='harga[]']").map(function(){return $(this).val();}).get();
            totalQtys = $("input[name='quantity[]']").map(function(){return $(this).val();}).get();

            totalHarga = 0
            totalQty = 0
            for (var i = 0; i < totalHargas.length; i++) {
                totalHarga += parseInt(totalHargas[i]) * parseInt(totalQtys[i]);
                totalQty += parseInt(totalQtys[i]);
            }

            get_all_total = totalHarga;
            $('#sum_qty').val(totalQty);
            $("input[name='total_harga']").val(get_all_total)
        }

        $('.qty-check').keyup(function (e) {
            e.preventDefault()
            let sum_qty = $(this).data('qty')
            let input_qty = $(this).val()

            if (input_qty > sum_qty) {
                swal("Quantity over limit!", "Not enough quantity! quantity available : " + sum_qty ,"warning")
                $(this).val("")
            }
        });

        function titleCase(string) {
            var sentence = string.toLowerCase().split(" ");
            for(var i = 0; i< sentence.length; i++){
                sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
            }
            return sentence;
        }
    </script>
@endsection
