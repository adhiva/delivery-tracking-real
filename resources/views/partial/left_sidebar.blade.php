{{-- Make simple each the menu --}}
@if(isset($menu['headerModule']))
    <li class="nav-small-cap">{{ $menu['headerModule'] }}</li>
@endif

@if (auth()->user()->can($menu['modulCode']))
    <li>
        {{--Validate the menu have a child or not  --}}
        @if(isset($menu['url']))
            @php ($href = "href=".url($menu['url'])."") @endphp
            @php ($classArrow = "") @endphp
        @else
            @php ($href = "href='javascript:void(0);'") @endphp
            @php ($classArrow = "has-arrow") @endphp
        @endif

        <a {{ $href }} class="{{ $classArrow }} waves-effect waves-dark" aria-expanded="false">
            @isset($menu['icon'])
                <i class="mdi {{ $menu['icon'] }}"></i>
            @endisset
            @if(isset($menu['title']))
                <span class="hide-menu">{{ $menu['title'] }}</span>
            @endif
        </a>

        {{-- When have a child each this --}}
        @isset($menu['subMenu'])
            <ul aria-expanded="false" class="collapse">
                @each('partial.left_sidebar', $menu['subMenu'], 'menu')
            </ul>
        @endisset
    </li>
@endif
