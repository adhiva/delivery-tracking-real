@component('mail::message')
Yth. Admin TJP Berikut terlampir bukti pengiriman dengan nomor STTB : **{{$sttb_number}}** {{-- use double space for line break --}}

<img src="{{ $file_name }}" alt="No Images">
Best Regards,  
**{{$name}}**.
@endcomponent