@extends('home')

@section('content_body')
    @role('admin')
        @section('headerPages', 'Dashboard Admin')
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-info"><i class="fa fas fa-tags"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-light">{{ $transactionProcess }}</h3>
                                <h5 class="text-muted m-b-0">On Process Delivery</h5></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-warning"><i class="fa fas fa-check"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-lgiht">{{ $transactionSuccess }}</h3>
                                <h5 class="text-muted m-b-0">Delivery Success By Day</h5></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-primary"><i class="fa fas fa-truck"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-lgiht">{{ $serviceVehicle }}</h3>
                                <h5 class="text-muted m-b-0">Vehicle in Service</h5></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column --> 
        </div>
    @else
        @section('headerPages', 'Dashboard Courier')
        <div class="row">
            <!-- Column -->
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-info"><i class="fa fas fa-tags"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-light">{{ $transactionProcess }}</h3>
                                <h5 class="text-muted m-b-0">Delivery Assigned</h5></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-warning"><i class="fa fas fa-check"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-lgiht">{{ $transactionSuccess }}</h3>
                                <h5 class="text-muted m-b-0">Delivery Success by Day</h5></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
    @endrole
@endsection