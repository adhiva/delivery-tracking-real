@extends('app')

@section('title', 'Login')

@section('body')
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url({{ asset('assets/images/background/login-delivery-project.jpg') }});">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-control-line" id="loginform" action="{{ route('login') }}" method="POST">
                        @csrf

                        <h3 class="box-title m-b-20">Sign In</h3>
                        <div class="form-group @error('email') has-danger @enderror">
                            <div class="col-xs-12">
                                <input name="email" id="email" class="form-control @error('email') form-control-danger @enderror" type="text" required="" value="{{ old('email') }}" placeholder="Username"> 
                                
                                {{-- When login error show this feedback --}}
                                @error('email')
                                    <div class="form-control-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                                
                            </div>
                        </div>
                        <div class="form-group @error('password') has-danger @enderror">
                            <div class="col-xs-12">
                                <input name="password" id="password" class="form-control @error('password') form-control-danger @enderror" type="password" required="" placeholder="Password" autocomplete="current-password"> 
                                @error('password')
                                    <div class="form-control-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="d-flex no-block align-items-center">
                                <div class="checkbox checkbox-primary p-t-0">
                                    <input name="remember" id="remember" id="checkbox-signup" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="checkbox-signup"> Remember me </label>
                                </div> 
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                            Don't have an account? <a href="{{ route('register') }}" class="text-info m-l-5"><b>Sign Up</b></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
@endsection