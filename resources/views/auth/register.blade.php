@extends('app')

@section('body')
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url({{ asset('assets/images/background/login-delivery-project.jpg') }});">        
            <div class="login-box card">
            <div class="card-body">
                <form method="POST" class="form-horizontal form-material" id="loginform" action="{{ route('register') }}">
                    @csrf
                    
                    <h3 class="box-title m-b-20">Sign Up</h3>
                    <div class="form-group @error('name') has-danger @enderror">
                        <div class="col-xs-12">
                            <input id="name" name="name" class="form-control @error('name') form-control-danger @enderror" type="text" required="" placeholder="Name">
                            @error('name')
                                <div class="form-control-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group @error('email') has-danger @enderror">
                        <div class="col-xs-12">
                            <input id="email" name="email" class="form-control @error('email') form-control-danger @enderror" type="text" required="" placeholder="Email">
                            @error('email')
                                <div class="form-control-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group @error('password') has-danger @enderror">
                        <div class="col-xs-12">
                            <input id="password" name="password" class="form-control @error('password') form-control-danger @enderror" type="password" required="" placeholder="Password">
                            @error('password')
                                <div class="form-control-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input id="password-confirm" name="password_confirmation" class="form-control" type="password" required="" placeholder="Confirm Password">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Sign Up</button>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            Already have an account? <a href="{{ route('login') }}" class="text-info m-l-5"><b>Sign In</b></a>
                        </div>
                    </div>
                </form>
                
            </div>
          </div>
        </div>
        
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
@endsection
