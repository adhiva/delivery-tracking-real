<title>{{ $data->do_number }}</title>
<style>
    #invoice{
        padding: 30px;
    }

    .invoice {
        position: relative;
        background-color: #FFF;
        min-height: 680px;
        padding: 15px
    }

    .invoice header {
        padding: 10px 0;
        margin-bottom: 20px;
        border-bottom: 1px solid #3989c6
    }

    .invoice .company-details {
        text-align: right
    }

    .invoice .company-details .name {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .contacts {
        margin-bottom: 20px
    }

    .invoice .invoice-to {
        text-align: left
    }

    .invoice .invoice-to .to {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .invoice-details {
        text-align: right
    }

    .invoice .invoice-details .invoice-id {
        margin-top: 0;
        color: #3989c6
    }

    .invoice main {
        padding-top: 200px;
        padding-bottom: 50px
    }

    .invoice main .thanks {
        margin-top: -100px;
        font-size: 2em;
        margin-bottom: 50px
    }

    .invoice main .notices {
        padding-left: 6px;
        border-left: 6px solid #3989c6
    }

    .invoice main .notices .notice {
        font-size: 1.2em
    }

    .column-kiri {
        float: left;
        width: 20%;
        height: 300px; /* Should be removed. Only for demonstration */
        padding-left: -80px;
    }
    .column-tengah {
        float: left;
        width: 60%;
        height: 300px; /* Should be removed. Only for demonstration */
    }
    .column-kanan {
        float: right;
        width: 50%;
        height: 300px; /* Should be removed. Only for demonstration */
        padding-right: -130px;
    }

    .column-kiri-sub {
        float: left;
        width: 40%;
        height: 300px; /* Should be removed. Only for demonstration */
        padding-left:-500px;
    }
    .column-kanan-sub {
        float: right;
        width: 40%;
        height: 300px; /* Should be removed. Only for demonstration */
        padding-right: -250px;
    }


    @media print {
        .invoice {
            font-size: 11px!important;
            overflow: hidden!important
        }

        .invoice footer {
            position: absolute;
            bottom: 10px;
            page-break-after: always
        }

        .invoice>div:last-child {
            page-break-before: always
        }
    }
</style>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!--Author      : @arboshiki-->
<div id="invoice">
    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="column-kiri" style="margin-right: 30px">
                        <img src="{{ asset('assets/images/users/logo.jpeg') }}" data-holder-rendered="true" />
                    </div>

                    <div class="column-tengah" style="text-align: center;">
                        <h2>Form</h2>
                        <h2>Internal Delivery Order/Note</h2>
                        <h2>PT Tunggal Jaya Plastic Industries</h2>
                    </div>

                    <div class="column-kanan">
                        <table>
                            <tr>
                                <td>DO Number</td>
                                <td>: </td>
                                <td>{{ $data->do_number }}</td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>: </td>
                                <td>{{ date("Y-m-d", strtotime($data->created_at)) }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </header>
            <main>
                <div class="row">
                    <div class="column-kiri-sub">
                        <table>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td>{{ date("Y-m-d", strtotime($data->created_at)) }}</td>
                            </tr>
                            <tr>
                                <td>Delivered To</td>
                                <td>:</td>
                                <td>{{ $data->client_company_name }}</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>:</td>
                                <td>{{ $data->address }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="column-kanan-sub">
                        <table>
                            <tr>
                                <td>Vehicle</td>
                                <td>:</td>
                                <td>{{ $data->type }}</td>
                            </tr>
                            <tr>
                                <td>Courier Name</td>
                                <td>:</td>
                                <td>{{ $data->courier_name }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="padding-top:200px;">
                    <table width=100%>
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Item</td>
                                <td>Quantity</td>
                                <td>Satuan</td>
                                <td>Price</td>
                                <td>Total</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($detailTransaction as $key => $transaction)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $transaction->item_name }}</td>
                                    <td>{{ $transaction->quantity }}</td>
                                    <td>Karton</td>
                                    <td>{{ $transaction->price }}</td>
                                    <td>{{ $transaction->price * $transaction->quantity }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </main>
            <footer>
                <div style="padding-left: -70px;">
                    <table width="100%" >
                        <tr style="text-align: center">
                            <td colspan="4">Diproses Oleh : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td colspan="2">Diserahkan Oleh : </td>
                            <td colspan="2">Diperiksa Oleh : </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height:50px;"></td>
                            <td colspan="2" style="height:50px;"></td>
                            <td colspan="2" style="height:50px;"></td>
                            <td colspan="2" style="height:50px;"></td>
                        </tr>
                        <tr style="text-align: center">
                            <td colspan="2">_______________</td>
                            <td colspan="2">_______________</td>
                            <td colspan="2">_______________</td>
                            <td colspan="2">_______________</td>
                        </tr>
                        <tr style="text-align: center">
                            <td colspan="2">Penjualan</td>
                            <td colspan="2">Pengiriman</td>
                            <td colspan="2">Gudang</td>
                            <td colspan="2">Satpam</td>
                        </tr>
                    </table>
                </div>
            </footer>
            {{-- <main>
                <div class="row contacts">
                    <div class="col invoice-to">
                        <div class="text-gray-light">INVOICE TO:</div>
                        <h2 class="to">John Doe</h2>
                        <div class="address">796 Silver Harbour, TX 79273, US</div>
                        <div class="email"><a href="mailto:john@example.com">john@example.com</a></div>
                    </div>
                    <div class="col invoice-details">
                        <h1 class="invoice-id">INVOICE 3-2-1</h1>
                        <div class="date">Date of Invoice: 01/10/2018</div>
                        <div class="date">Due Date: 30/10/2018</div>
                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-left">DESCRIPTION</th>
                            <th class="text-right">HOUR PRICE</th>
                            <th class="text-right">HOURS</th>
                            <th class="text-right">TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="no">04</td>
                            <td class="text-left"><h3>
                                <a target="_blank" href="https://www.youtube.com/channel/UC_UMEcP_kF0z4E6KbxCpV1w">
                                Youtube channel
                                </a>
                                </h3>
                               <a target="_blank" href="https://www.youtube.com/channel/UC_UMEcP_kF0z4E6KbxCpV1w">
                                   Useful videos
                               </a>
                               to improve your Javascript skills. Subscribe and stay tuned :)
                            </td>
                            <td class="unit">$0.00</td>
                            <td class="qty">100</td>
                            <td class="total">$0.00</td>
                        </tr>
                        <tr>
                            <td class="no">01</td>
                            <td class="text-left"><h3>Website Design</h3>Creating a recognizable design solution based on the company's existing visual identity</td>
                            <td class="unit">$40.00</td>
                            <td class="qty">30</td>
                            <td class="total">$1,200.00</td>
                        </tr>
                        <tr>
                            <td class="no">02</td>
                            <td class="text-left"><h3>Website Development</h3>Developing a Content Management System-based Website</td>
                            <td class="unit">$40.00</td>
                            <td class="qty">80</td>
                            <td class="total">$3,200.00</td>
                        </tr>
                        <tr>
                            <td class="no">03</td>
                            <td class="text-left"><h3>Search Engines Optimization</h3>Optimize the site for search engines (SEO)</td>
                            <td class="unit">$40.00</td>
                            <td class="qty">20</td>
                            <td class="total">$800.00</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">SUBTOTAL</td>
                            <td>$5,200.00</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">TAX 25%</td>
                            <td>$1,300.00</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">GRAND TOTAL</td>
                            <td>$6,500.00</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="thanks">Thank you!</div>
                <div class="notices">
                    <div>NOTICE:</div>
                    <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
                </div>
            </main> --}}
            {{-- <footer>
                Invoice was created on a computer and is valid without the signature and seal.
            </footer> --}}
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
</div>
