@extends('home')

@section('headerPages', 'List Vehicle')

@section('content_body')
    <div class="card">
        <div class="card-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"> 
                    <a class="mytooltip nav-link active" data-toggle="tab" href="#listVehicle" role="tab">
                        <span>
                            <i data-toggle="tooltip" data-placement="top" title="Vehicle List" class="fa fas fa-list-alt"></i>
                        </span>
                    </a> 
                </li>
                <li class="nav-item"> 
                    <a class="nav-link" data-toggle="tab" href="#addVehicle" role="tab">
                        <span>
                            <i data-toggle="tooltip" data-placement="top" title="Add Vehicle" class="fa fas fa-plus"></i>
                        </span>
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content tabcontent-border">
                <div class="tab-pane active" id="listVehicle" role="tabpanel">
                    <div class="p-20">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <th>No</th>
                                    <th>Number Plate</th>
                                    <th>Merk</th>
                                    <th>Type</th>
                                    <th>Capacity</th>
                                    <th>Pcs</th>
                                    <th>Service Schedule at</th>
                                    <th>Action</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                {{-- Form Add --}}
                <div class="tab-pane  p-20" id="addVehicle" role="tabpanel">
                    <form class="form-horizontal m-t-40" id="formAddVehicle">
                        <div class="form-group">
                            <label>Number Plate</label>
                            <input type="text" name="number_plate" class="form-control" placeholder="Number Plate" value="" maxlength="8">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                        <div class="form-group">
                            <label>Merk</label>
                            <input type="text" name="merk" class="form-control" placeholder="Merk" value="" maxlength="50">
                        </div>
                        <div class="form-group">
                            <label>Type</label>
                            <input type="text" name="type" class="form-control" placeholder="Type" value="" maxlength="50">
                        </div>
                        <div class="form-group">
                            <label>Capacity</label>
                            <input type="number" name="capacity" class="form-control" placeholder="Capacity" value="" maxlength="5">
                        </div>
                        <div class="form-group">
                            <label>Pcs</label>
                            <input type="text" name="pcs" class="form-control" placeholder="Piecies, eg. : Karton" value="" maxlength="50">
                        </div>
                        <div class="form-group">
                            <label>Service Schedule</label>
                            <input type="text" name="service_schedule_monthly" class="form-control mdate" placeholder="Select date">
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                            <button type="button" class="btn btn-inverse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- Start Modal Here --}}
        {{-- Edit Vehicles Modal --}}
        <div id="updateModalVehicle" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Update Vehicle</span></h4>
                        <button type="button" class="close align-right" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="formUpdateVehicle">
                            <div class="form-group">
                                <label>Number Plate</label>
                                <input id="update_number_plate" type="text" name="number_plate" class="form-control" placeholder="Number Plate" value="" maxlength="8">
                            </div>
                            <div class="form-group">
                                <label>Merk</label>
                                <input id="update_merk" type="text" name="merk" class="form-control" placeholder="Merk" value="" maxlength="50">
                            </div>
                            <div class="form-group">
                                <label>Type</label>
                                <input id="update_type" type="text" name="type" class="form-control" placeholder="Type" value="" maxlength="50">
                            </div>
                            <div class="form-group">
                                <label>Capacity</label>
                                <input id="update_capacity" type="number" name="capacity" class="form-control" placeholder="Capacity" value="" maxlength="5">
                            </div>
                            <div class="form-group">
                                <label>Pcs</label>
                                <input id="update_pcs" type="text" name="pcs" class="form-control" placeholder="Piecies, eg. : Karton" value="" maxlength="50">
                            </div>
                            <div class="form-group">
                                <label>Service Schedule</label>
                                <input id="update_service_schedule_monthly" type="text" name="service_schedule_monthly" class="form-control mdate" placeholder="Select date">
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="button" id="editFormButton" class="btn btn-success">Update</button>
                                                <button type="button" class="btn btn-inverse" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('templateCSS')
    <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
    <style>
        .modal-dialog{
            max-width: 700px !important;
        }
    </style>
@endsection

@section('templateJS')
    <script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    {{-- Pretty tooltips --}}
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?lang=css&amp;skin=default"></script>
    <!-- end - This is for export functionality only -->
    
    {{-- Select2 --}}
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}" type="text/javascript"></script>

    {{-- Sweet Alert --}}
    <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js') }}"></script>

    <!-- Material Design Bootstrap Datepicker -->
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    <script>
        $( document ).ready(function() {
            // datepicker material design
            $('.mdate').bootstrapMaterialDatePicker(
                { 
                    weekStart: 0, 
                    time: false
                }
            );

            var table = $('#myTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('list.vehicle-datatable') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'number_plate', name: 'number_plate'},
                    {data: 'merk', name: 'merk'},
                    {data: 'type', name: 'type'},
                    {data: 'capacity', name: 'capacity'},
                    {data: 'pcs', name: 'pcs'},
                    {data: 'service_schedule_monthly', name: 'service_schedule_monthly'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $('#formAddVehicle').submit(function (e) { 
                e.preventDefault();
                data = $(this).serialize();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });
                
                $.ajax({
                    type: "POST",
                    url: "{{ route('store.vehicle') }}",
                    data: data,
                    dataType: "JSON",
                    success: function (response) {
                        swal("Success!", response.message, "success")
                        document.getElementById("formAddVehicle").reset();
                        $('#myTable').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error ! ", "Internal Server Error !", "error")
                    }
                });
            });

        });

        function updateVehicle(vehicleId) {
            let vehiclesData = getData(vehicleId)
            $('#updateModalVehicle').modal('show');
            $('#update_number_plate').val(vehiclesData.number_plate);
            $('#update_merk').val(vehiclesData.merk);
            $('#update_type').val(vehiclesData.type);
            $('#update_capacity').val(vehiclesData.capacity);
            $('#update_pcs').val(vehiclesData.pcs);
            $('#update_service_schedule_monthly').val(vehiclesData.service_schedule_monthly);

            // Action for updating
            $('#editFormButton').off();
            $('#editFormButton').click(function (e) { 
                e.preventDefault();
                e.stopPropagation();

                let url = '{{ route("update.vehicle", ":id") }}';
                url = url.replace(':id', vehicleId);
                data = $('#formUpdateVehicle').serialize();
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });

                $.ajax({
                    type: "PUT",
                    url: url,
                    data: data,
                    dataType: "JSON",
                    success: function (response) {
                        swal("Success!", response.message, "success")
                        $('#myTable').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error ! ", "Internal Server Error !", "error")
                    }
                });
            });
        }

        function removeVehicle(vehicleId) {
            let url = '{{ route("destroy.vehicle", ":id") }}';
            url = url.replace(':id', vehicleId);

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                     $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        dataType: "JSON",
                        success: function (response) {
                            $('#myTable').DataTable().ajax.reload();
                            Swal.fire(
                                'Deleted!',
                                response.message,
                                'success'
                            )
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            Swal.fire(
                                'Error',
                                'Internal Server Error!',
                                'warning'
                            )
                        }
                    });
                    
                } else {
                    Swal.fire(
                        'Safe!',
                        'Your data has been safe.',
                        'success'
                    )
                }
            })
        }

        // Getting data according page
        function getData(vehicleId) {
            let url = '{{ route("get.vehicle", ":id") }}';
            url = url.replace(':id', vehicleId);
            
            let vehicles = "";
            $.ajax({
                type: "GET",
                url: url,
                dataType: "JSON",
                async: false,
                success: function (response) {
                    vehicles = response.data;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error ! ", "Internal Server Error !", "error")
                }
            });

            return vehicles;
        }

        function titleCase(string) {
            var sentence = string.toLowerCase().split(" ");
            for(var i = 0; i< sentence.length; i++){
                sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
            }
            return sentence;
        }
    </script>
@endsection