@extends('home')

@section('headerPages', 'Courier Management')

@section('content_body')
    <div class="card">
        <div class="card-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"> 
                    <a class="mytooltip nav-link active" data-toggle="tab" href="#listCourier" role="tab">
                        <span>
                            <i data-toggle="tooltip" data-placement="top" title="Courier List" class="fa fas fa-list-alt"></i>
                        </span>
                    </a> 
                </li>
                <li class="nav-item"> 
                    <a class="nav-link" data-toggle="tab" href="#addCourier" role="tab">
                        <span>
                            <i data-toggle="tooltip" data-placement="top" title="Add Courier" class="fa fas fa-plus"></i>
                        </span>
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content tabcontent-border">
                <div class="tab-pane active" id="listCourier" role="tabpanel">
                    <div class="p-20">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Gender</th>
                                        <th>Date Of Birth</th>
                                        <th>Address</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane  p-20" id="addCourier" role="tabpanel">
                    <form class="form-horizontal m-t-40" id="formAddCourier">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Name" value="" maxlength="50">
                        </div>
                        <div class="form-group">
                            <label>Number SIM</label>
                            <input type="text" name="number_sim" class="form-control" placeholder="Number SIM" value="" maxlength="20">
                        </div>
                        <div class="form-group">
                            <label>Number Identity</label>
                            <input type="text" name="number_identity" class="form-control" placeholder="Number Identity" value="" maxlength="20">
                        </div>
                        <div class="form-group">
                            <input name="gender" name="gender" type="radio" id="radio_30" class="with-gap radio-col-light-blue" value="M" />
                            <label for="radio_30">Male</label>
                            <input name="gender" name="gender" type="radio" id="radio_31" class="with-gap radio-col-pink" value="F" />
                            <label for="radio_31">Female</label>
                        </div>
                        <div class="form-group">
                            <label>Date Of Birth</label>
                            <input type="text" name="date_of_birth" class="form-control mdate" placeholder="Date Of Birth">
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea class="form-control" rows="5" name="address" ></textarea>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                            <button type="button" class="btn btn-inverse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        {{-- Edit Courier Modal --}}
        <div id="updateModalCourier" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Update Courier</span></h4>
                        <button type="button" class="close align-right" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="formUpdateCourier">
                            <div class="form-group">
                                <label>Name</label>
                                <input id="update_name" type="text" name="name" class="form-control" placeholder="Name" value="" maxlength="50">
                            </div>
                            <div class="form-group">
                                <label>Number SIM</label>
                                <input id="update_number_sim" type="text" name="number_sim" class="form-control" placeholder="Number SIM" value="" maxlength="20">
                            </div>
                            <div class="form-group">
                                <label>Number Identity</label>
                                <input id="update_number_identity" type="text" name="number_identity" class="form-control" placeholder="Number Identity" value="" maxlength="20">
                            </div>
                            <div class="form-group">
                                <input id="update_gender_male" name="gender" name="gender" type="radio" class="with-gap radio-col-light-blue" value="M" />
                                <label for="update_gender_male">Male</label>
                                <input id="update_gender_female" name="gender" name="gender" type="radio" class="with-gap radio-col-pink" value="F" />
                                <label for="update_gender_female">Female</label>
                            </div>
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input id="update_date_of_birth" type="text" name="date_of_birth" class="form-control mdate" placeholder="Date Of Birth" >
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <textarea id="update_address" class="form-control" rows="5" name="address" ></textarea>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="button" id="updateCourierButton" class="btn btn-success">Update</button>
                                                <button type="button" class="btn btn-inverse" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('templateCSS')
    {{-- Sweet Alert --}}
    <link href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">

    {{-- Material Date Time Picker --}}
    <link href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
@endsection

@section('templateJS')
    <script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    {{-- Sweet Alert --}}
    <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js') }}"></script>

    <!-- Material Design Bootstrap Datepicker -->
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    {{-- Pretty tooltips --}}
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?lang=css&amp;skin=default"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $( document ).ready(function() {
            // datepicker material design
            $('.mdate').bootstrapMaterialDatePicker(
                { 
                    weekStart: 0, 
                    time: false
                }
            );

            var table = $('#myTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('list.courier-datatable') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'gender', name: 'gender', orderable: false, searchable: false},
                    {data: 'date_of_birth', name: 'date_of_birth'},
                    {data: 'address', name: 'address'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $('#formAddCourier').submit(function (e) { 
                e.preventDefault();
                data = $(this).serialize();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });
                
                $.ajax({
                    type: "POST",
                    url: "{{ route('store.courier') }}",
                    data: data,
                    dataType: "JSON",
                    success: function (response) {
                        swal("Success!", response.message, "success")
                        document.getElementById("formAddCourier").reset();
                        $('#myTable').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error ! ", "Internal Server Error !", "error")
                    }
                });
            });
        });

        function updateCourier(courierId) {
            let courierData = getData(courierId)
            $('#updateModalCourier').modal('show');
            $('#update_name').val(courierData.name);
            $('#update_number_sim').val(courierData.number_sim);
            $('#update_number_identity').val(courierData.number_identity);
            let gender = courierData.gender;
            if (gender == "M") {
                $('#update_gender_male').attr("checked", true);
                $('#update_gender_female').attr("checked", false);
            } else {
                $('#update_gender_female').attr("checked", true);
                $('#update_gender_male').attr("checked", false);
            }
            
            $('#update_date_of_birth').val(courierData.date_of_birth);
            $('#update_address').val(courierData.address);

            // Action for updating
            $('#updateCourierButton').off();
            $('#updateCourierButton').click(function (e) { 
                e.preventDefault();
                e.stopPropagation();
                
                let url = '{{ route("update.courier", ":id") }}';
                url = url.replace(':id', courierId);
                data = $('#formUpdateCourier').serialize();
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });

                $.ajax({
                    type: "PUT",
                    url: url,
                    data: data,
                    dataType: "JSON",
                    success: function (response) {
                        swal("Success!", response.message, "success")
                        $('#myTable').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error ! ", "Internal Server Error !", "error")
                    }
                });
            });
        }

        // Remove Courier
        function removeCourier(courierId) {
            let url = '{{ route("destroy.courier", ":id") }}';
            url = url.replace(':id', courierId);

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                     $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        dataType: "JSON",
                        success: function (response) {
                            $('#myTable').DataTable().ajax.reload();
                            Swal.fire(
                                'Deleted!',
                                response.message,
                                'success'
                            )
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            Swal.fire(
                                'Error',
                                'Internal Server Error!',
                                'warning'
                            )
                        }
                    });
                    
                } else {
                    Swal.fire(
                        'Safe!',
                        'Your data has been safe.',
                        'success'
                    )
                }
            })
        }

        // Getting data according page
        function getData(courierId) {
            let url = '{{ route("get.courier", ":id") }}';
            url = url.replace(':id', courierId);
            
            let couriers = "";
            $.ajax({
                type: "GET",
                url: url,
                dataType: "JSON",
                async: false,
                success: function (response) {
                    couriers = response.data;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error ! ", "Internal Server Error !", "error")
                }
            });

            return couriers;
        }
    </script>
@endsection