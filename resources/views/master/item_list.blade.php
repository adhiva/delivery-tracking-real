@extends('home')

@section('headerPages', 'Item Management')

@section('content_body')
    <div class="card">
        <div class="card-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"> 
                    <a class="mytooltip nav-link active" data-toggle="tab" href="#listItem" role="tab">
                        <span>
                            <i data-toggle="tooltip" data-placement="top" title="Item List" class="fa fas fa-list-alt"></i>
                        </span>
                    </a> 
                </li>
                <li class="nav-item"> 
                    <a class="nav-link" data-toggle="tab" href="#addItem" role="tab">
                        <span>
                            <i data-toggle="tooltip" data-placement="top" title="Add Item" class="fa fas fa-plus"></i>
                        </span>
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content tabcontent-border">
                <div class="tab-pane active" id="listItem" role="tabpanel">
                    <div class="p-20">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane  p-20" id="addItem" role="tabpanel">
                    <form class="form-horizontal m-t-40" id="formAddItem">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Name" maxlength="50">
                        </div>
                        <div class="form-group">
                            <label>Price</label>
                            <input type="text" id="price_real" name="price_format" class="form-control price-format" placeholder="Price" maxlength="10">
                        </div>
                        <div class="form-group">
                            <label>Quantity</label>
                            <input type="number" name="quantity" class="form-control" placeholder="Quantity" maxlength="5">
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                            <button type="button" class="btn btn-inverse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- Edit Item Modal --}}
        <div id="updateModalItem" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Update Item</span></h4>
                        <button type="button" class="close align-right" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="formUpdateItem">
                            <div class="form-group">
                                <label>Name</label>
                                <input id="update_name" type="text" name="name" class="form-control" placeholder="Name" maxlength="50">
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input id="update_price" type="text" name="price" class="form-control price-format" placeholder="Price" maxlength="10">
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <input id="update_qty" type="number" name="quantity" class="form-control" placeholder="Quantity" maxlength="5">
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="button" id="editForm" class="btn btn-success">Update</button>
                                                <button type="button" class="btn btn-inverse" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('templateCSS')
    {{-- Sweet Alert --}}
    <link href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">

    {{-- Material Date Time Picker --}}
    <link href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
@endsection

@section('templateJS')
    <script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    {{-- Pretty tooltips --}}
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?lang=css&amp;skin=default"></script>
    <!-- end - This is for export functionality only -->

    {{-- Sweet Alert --}}
    <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js') }}"></script>

    <!-- Material Design Bootstrap Datepicker -->
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    {{-- Start script for this page only --}}
    <script>
        $( document ).ready(function() {
            var table = $('#myTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('list.item-datatable') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {
                        data: 'price', 
                        render: function ( data, type, row ) {
                            return "Rp. " + formatRupiah(data);
                        }
                    },
                    {data: 'quantity', name:'quantity'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $('#formAddItem').submit(function (e) { 
                e.preventDefault();
                data = $(this).serialize();
                realPrice = $('#price_real').val();
                data += "&price=" + revertRupiah(realPrice);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });
                
                $.ajax({
                    type: "POST",
                    url: "{{ route('store.item') }}",
                    data: data,
                    dataType: "JSON",
                    success: function (response) {
                        swal("Success!", response.message, "success")
                        document.getElementById("formAddItem").reset();
                        $('#myTable').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error ! ", "Internal Server Error !", "error")
                    }
                });
            });
        });

        function updateItem(itemId) {
            let itemData = getData(itemId)
            $('#updateModalItem').modal('show');
            
            formatPrice = formatRupiah(itemData.price)
            $('#update_name').val(itemData.name);
            $('#update_price').val(formatPrice);
            $('#update_qty').val(itemData.quantity);

            $('#editForm').off();
            $('#editForm').click(function (e) { 
                e.preventDefault();
                e.stopPropagation();
                
                let url = '{{ route("update.item", ":id") }}';
                url = url.replace(':id', itemId);
                data = $('#formUpdateItem').serialize();
                realPrice = $('#update_price').val();
                data += "&price=" + revertRupiah(realPrice);
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });

                $.ajax({
                    type: "PUT",
                    url: url,
                    data: data,
                    dataType: "JSON",
                    success: function (response) {
                        swal("Success!", response.message, "success")
                        e.stopImmediatePropagation();
                        $('#myTable').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error ! ", "Internal Server Error !", "error")
                    }
                });
            });
        }

        // Remove Item
        function removeItem(itemId) {
            let url = '{{ route("destroy.item", ":id") }}';
            url = url.replace(':id', itemId);

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                     $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        dataType: "JSON",
                        success: function (response) {
                            $('#myTable').DataTable().ajax.reload();
                            Swal.fire(
                                'Deleted!',
                                response.message,
                                'success'
                            )
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            Swal.fire(
                                'Error',
                                'Internal Server Error!',
                                'warning'
                            )
                        }
                    });
                    
                } else {
                    Swal.fire(
                        'Safe!',
                        'Your data has been safe.',
                        'success'
                    )
                }
            })
        }

        // Getting data according page
        function getData(itemId) {
            let url = '{{ route("get.item", ":id") }}';
            url = url.replace(':id', itemId);
            
            let items = "";
            $.ajax({
                type: "GET",
                url: url,
                dataType: "JSON",
                async: false,
                success: function (response) {
                    items = response.data;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error ! ", "Internal Server Error !", "error")
                }
            });

            return items;
        }

        $('.price-format').keyup(function (e) { 
            let price = $(this).val();

            // Set price to rupiah format
            $(this).val(formatRupiah(price))
        });

        /* Fungsi formatRupiah */
		function formatRupiah(angka){
            
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return rupiah;
		}

        // revert rupiah to integer
        function revertRupiah(angka){
            console.log(angka);
			var angka = angka.replace('.', '')
            angka = parseInt(angka);
			return angka;
		}
    </script>
@endsection