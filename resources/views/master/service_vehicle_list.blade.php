@extends('home')

@section('headerPages', 'List Service Vehicle')

@section('content_body')
    <div class="card">
        <div class="card-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"> 
                    <a class="mytooltip nav-link active" data-toggle="tab" href="#listServiceVehicle" role="tab">
                        <span>
                            <i data-toggle="tooltip" data-placement="top" title="Service Vehicle List" class="fa fas fa-list-alt"></i>
                        </span>
                    </a> 
                </li>
                <li class="nav-item"> 
                    <a class="nav-link" data-toggle="tab" href="#addServiceVehicle" role="tab">
                        <span>
                            <i data-toggle="tooltip" data-placement="top" title="Add Service Vehicle" class="fa fas fa-plus"></i>
                        </span>
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content tabcontent-border">
                <div class="tab-pane active" id="listServiceVehicle" role="tabpanel">
                    <div class="p-20">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <th>No</th>
                                    <th>Complaint</th>
                                    <th>Car Type</th>
                                    <th>Car Merk</th>
                                    <th>Service Date</th>
                                    <th>Status</th>
                                    <th>Finish Service Date</th>
                                    <th>Courier Have Complaint</th>
                                    <th>Action</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                {{-- Form Add --}}
                <div class="tab-pane  p-20" id="addServiceVehicle" role="tabpanel">
                    <form class="form-horizontal m-t-40" id="formAddServiceVehicle">
                        <div class="form-group">
                            <label>Vehicle List</label>
                            <select name="vehicle_id" class="select2" style="width: 100%"></select>
                        </div>
                        <div class="form-group">
                            <label>Complaint</label>
                            <textarea class="form-control" rows="5" name="complaint" ></textarea>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                            <button type="button" class="btn btn-inverse">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- Start Modal Here --}}
        {{-- Edit ServiceVehicles Modal --}}
        <div id="updateModalServiceVehicle" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Update Service Vehicle</span></h4>
                        <button type="button" class="close align-right" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="formUpdateServiceVehicle">
                            <div class="form-group">
                                <label>Courier Name</label>
                                <input type="text" disabled id="update_courier_name" class="form-control" placeholder="Courier Name">
                            </div>

                            <div class="form-group">
                                <label>Vehicle Merk</label>
                                <input type="text" disabled id="update_merk" class="form-control" placeholder="Vehicle Merk">
                            </div>

                            <div class="form-group">
                                <label>Vehicle Type</label>
                                <input type="text" disabled id="update_type" class="form-control" placeholder="Vehicle Type">
                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <input type="text" disabled id="update_status_description" class="form-control" placeholder="Status">
                            </div>

                            <div class="form-group">
                                <label>Complaint</label>
                                <textarea id="update_complaint" class="form-control" rows="5" name="complaint" ></textarea>
                            </div>

                            @role('admin')
                                <div class="form-group" id="divUpdateDateOfService">
                                    <label>Date Of Service</label>
                                    <input id="update_service_date" type="text" name="service_date" class="form-control mdate" placeholder="Date Of Service">
                                </div>
                            @endrole
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-success">Update</button>
                                                <button type="button" class="btn btn-inverse" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- Update Status Service Approved Vehicles Modal --}}
        <div id="approveServiceModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Approve Status Vehicle</span></h4>
                        <button type="button" class="close align-right" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="formApproveServiceVehicle">
                            {{-- Start Form --}}
                            <div class="form-group">
                                <label>Complaint</label>
                                <textarea disabled id="approve_complaint" class="form-control" rows="5" name="complaint" ></textarea>
                            </div>

                            <div class="form-group">
                                <label>Vehicle Merk</label>
                                <input type="text" disabled id="approve_merk" class="form-control" placeholder="Vehicle Merk">
                            </div>

                            <div class="form-group">
                                <label>Vehicle Type</label>
                                <input type="text" disabled id="approve_type" class="form-control" placeholder="Vehicle Type">
                            </div>

                            <div class="form-group">
                                <label>Courier Name</label>
                                <input type="text" disabled id="approve_courier_name" class="form-control" placeholder="Courier Name">
                            </div>
                            
                            <div class="form-group">
                                <label>Status</label>
                                <input type="text" disabled id="approve_status_description" class="form-control" placeholder="Status">
                                <input type="hidden" id="approve_status" name="status" >
                            </div>

                            <div class="form-group">
                                <label>Date Of Service</label>
                                <input type="text" name="service_date" class="form-control mdate" placeholder="Date Of Service">
                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-success">Approve</button>
                                                <button type="button" class="btn btn-inverse" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('templateCSS')
    <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    
    {{-- Sweet Alert --}}
    <link href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">

    {{-- Material Date Time Picker --}}
    <link href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
    <style>
        .modal-dialog{
            max-width: 700px !important;
        }
    </style>
@endsection

@section('templateJS')
    <script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    {{-- Pretty tooltips --}}
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?lang=css&amp;skin=default"></script>
    <!-- end - This is for export functionality only -->
    
    {{-- Select2 --}}
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}" type="text/javascript"></script>

    {{-- Sweet Alert --}}
    <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js') }}"></script>

    <!-- Material Design Bootstrap Datepicker -->
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    <script>
        $( document ).ready(function() {
            // datepicker material design
            let date = new Date();
            date.setDate(date.getDate());
            $('.mdate').bootstrapMaterialDatePicker(
                { 
                    minDate:new Date(),
                    weekStart: 0, 
                    time: false
                }
            );

            // Init Select2
            $(".select2").select2({
                placeholder: 'Choose vehicle',
                ajax: {
                    url: '{{ route("select2.vehicle") }}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.type,
                                    id: item.id
                                }
                            })
                        };
                    },
                    cache: true
                }
            });

            var table = $('#myTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('list.service-datatable') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'complaint', name: 'complaint'},
                    {data: 'type', name: 'type'},
                    {data: 'merk', name: 'merk'},
                    {data: 'service_date', name: 'service_date'},
                    {
                        data: 'status', 
                        render: function ( data, type, row ) {

                            dataStatus = mappingStatus(data)
                            
                            colorStatus = dataStatus['colorStatus']
                            statusDesc = dataStatus['status']

                            let labelStatus = "<span class='label label-"+ colorStatus +"'>"+ statusDesc +"</span> ";
                            return labelStatus;
                        }
                    },
                    {data: 'finish_service_date', name: 'finish_service_date'},
                    {data: 'courier_name', name: 'courier_name'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $('#formAddServiceVehicle').submit(function (e) { 
                e.preventDefault();
                data = $(this).serialize();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });
                
                $.ajax({
                    type: "POST",
                    url: "{{ route('store.service') }}",
                    data: data,
                    dataType: "JSON",
                    success: function (response) {
                        swal("Success!", response.message, "success")
                        document.getElementById("formAddServiceVehicle").reset();
                        $('#myTable').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error ! ", "Internal Server Error !", "error")
                    }
                });
            });

        });

        function approveServiceVehicle(serviceVehicleId) {
            let serviceVehicleData = getData(serviceVehicleId)
            $('#approveServiceModal').modal('show');
            
            $('#approve_complaint').val(serviceVehicleData.complaint);
            $('#approve_courier_name').val(serviceVehicleData.courier_name);
            $('#approve_merk').val(serviceVehicleData.merk);
            $('#approve_type').val(serviceVehicleData.type);
            let getStatusDesc = mappingStatus(serviceVehicleData.status);
            $('#approve_status_description').val(getStatusDesc['status']);
            $('#approve_status').val(serviceVehicleData.status);

            // Action for approve service
            $('#formApproveServiceVehicle').submit(function (e) { 
                e.preventDefault();
                e.stopPropagation();
                $(this).off('click');
                let url = '{{ route("update.service-status", ":id") }}';
                url = url.replace(':id', serviceVehicleId);
                data = $(this).serialize();
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });

                $.ajax({
                    type: "PUT",
                    url: url,
                    data: data,
                    dataType: "JSON",
                    success: function (response) {
                        swal("Success!", response.message, "success")
                        $('#myTable').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error ! ", "Internal Server Error !", "error")
                    }
                });
            });
        }

        function updateServiceVehicle(serviceVehicleId) {
            let serviceVehicleData = getData(serviceVehicleId)
            $('#updateModalServiceVehicle').modal('show');
            $('#update_complaint').val(serviceVehicleData.complaint);
            $('#update_courier_name').val(serviceVehicleData.courier_name);
            $('#update_merk').val(serviceVehicleData.merk);
            $('#update_type').val(serviceVehicleData.type);
            let getStatusDesc = mappingStatus(serviceVehicleData.status);
            $('#update_status_description').val(getStatusDesc['status']);
            
            $('#divUpdateDateOfService').removeClass("hide");
            $('#divUpdateDateOfService').attr("disabled", false);
            if (serviceVehicleData.status === 0) {
                $('#divUpdateDateOfService').addClass("hide");
                $('#divUpdateDateOfService').attr("disabled", true);
            }
            $('#update_service_date').val(serviceVehicleData.service_date);

            // Action for updating
            $('#formUpdateServiceVehicle').submit(function (e) { 
                e.preventDefault();
                e.stopPropagation();
                $(this).off('click');
                let url = '{{ route("update.service", ":id") }}';
                url = url.replace(':id', serviceVehicleId);
                data = $(this).serialize();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });

                $.ajax({
                    type: "PUT",
                    url: url,
                    data: data,
                    dataType: "JSON",
                    success: function (response) {
                        swal("Success!", response.message, "success")
                        $('#myTable').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error ! ", "Internal Server Error !", "error")
                    }
                });
            });
        }

        function updateStatusToFinishService(serviceVehicleId) {
            let url = '{{ route("update.service-status", ":id") }}';
                url = url.replace(':id', serviceVehicleId);
            let serviceVehicleData = getData(serviceVehicleId)

            data = {
                status : serviceVehicleData.status
            }

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't revert this data after finish this service vehicle!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, finish it!'
            }).then((result) => {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        }
                    });

                     $.ajax({
                        type: "PUT",
                        url: url,
                        data: data,
                        dataType: "JSON",
                        success: function (response) {
                            $('#myTable').DataTable().ajax.reload();
                            Swal.fire(
                                'Service vehicle finish!',
                                response.message,
                                'success'
                            )
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            Swal.fire(
                                'Error',
                                'Internal Server Error!',
                                'warning'
                            )
                        }
                    });
                    
                } else {
                    Swal.fire(
                        'It will be finish later!',
                        'Your vehicle not finish service!',
                        'success'
                    )
                }
            })
        }

        function removeServiceVehicle(serviceVehicleId) {
            let url = '{{ route("destroy.service", ":id") }}';
            url = url.replace(':id', serviceVehicleId);

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                     $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        dataType: "JSON",
                        success: function (response) {
                            $('#myTable').DataTable().ajax.reload();
                            Swal.fire(
                                'Deleted!',
                                response.message,
                                'success'
                            )
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            Swal.fire(
                                'Error',
                                'Internal Server Error!',
                                'warning'
                            )
                        }
                    });
                    
                } else {
                    Swal.fire(
                        'Safe!',
                        'Your data has been safe.',
                        'success'
                    )
                }
            })
        }

        // Getting data according page
        function getData(clientCompanyId) {
            let url = '{{ route("get.service", ":id") }}';
            url = url.replace(':id', clientCompanyId);
            
            let clientCompanies = "";
            $.ajax({
                type: "GET",
                url: url,
                dataType: "JSON",
                async: false,
                success: function (response) {
                    clientCompanies = response.data;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error ! ", "Internal Server Error !", "error")
                }
            });

            return clientCompanies;
        }

        function mappingStatus(status) { 
            let mapStatus = [];
            switch (status) {
                case 0:
                    mapStatus['status'] = "Propose"
                    mapStatus['colorStatus'] = "danger"
                    break;
                case "1":
                    mapStatus['status'] = "Approval and Process Fix car"
                    mapStatus['colorStatus'] = "info"
                    break;
                case "2":
                    mapStatus['status'] = "Finish"
                    mapStatus['colorStatus'] = "success"
                    break;
                default:
                    mapStatus['status'] = "Not Detected"
                    mapStatus['colorStatus'] = "warning"
                    break;
            }

            return mapStatus
        }

        function titleCase(string) {
            var sentence = string.toLowerCase().split(" ");
            for(var i = 0; i< sentence.length; i++){
                sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
            }
            return sentence;
        }
    </script>
@endsection